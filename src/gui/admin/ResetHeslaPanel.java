package gui.admin;

import gui.MainFrame;
import gui.util.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static model.Zamestnanec.resetPw;

public class ResetHeslaPanel extends JPanel {
    public ResetHeslaPanel(MainFrame mainFrame) {
        super(new SpringLayout(),false);
        GridBagConstraints c = new GridBagConstraints();

        JLabel resetLoginLabel = new JLabel("Login: ");
        JTextField resetLoginField = new JTextField();

        JLabel pwTypeResetLabel = new JLabel("Typ hesla:");
        String[] typesOfPw = {"123456(první písmeno ze Jména a Prijmení)","6 náhodných čísel(první písmeno ze Jména a Prijmení)","8 náhodných čísel"};
        JComboBox<String> pwTypeResetCombo = new JComboBox<>(typesOfPw);

        JLabel emptyLabel = new JLabel("");
        JButton resetButton = new JButton("Resetovat heslo");

        add(resetLoginLabel, c);
        add(resetLoginField, c);
        resetLoginField.setMaximumSize(resetLoginField.getPreferredSize());

        add(pwTypeResetLabel, c);
        add(pwTypeResetCombo, c);
        pwTypeResetCombo.setMaximumSize(pwTypeResetCombo.getPreferredSize());

        add(emptyLabel, c);
        add(resetButton, c);

        SpringUtilities.makeCompactGrid(this,
                3, 2,  //rows, cols
                6, 6,  //initX, initY
                6, 6); //xPad, yPad

        resetButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                String newPw = resetPw(resetLoginField.getText(), pwTypeResetCombo.getSelectedIndex());
                if(newPw.equals("Špatný login"))
                    mainFrame.getAdminPanel().getNewUserOutPanel().setYourLogin(newPw, newPw);
                else
                    mainFrame.getAdminPanel().getNewUserOutPanel().setYourLogin(resetLoginField.getText(), newPw);
            }
        });
    }
}
