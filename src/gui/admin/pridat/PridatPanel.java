package gui.admin.pridat;

import gui.MainFrame;
import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PridatPanel extends JPanel {
    public PridatPanel(MainFrame mainFrame, User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel pridatZazemiLabel = new JLabel("Přidat Zázemí");
        pridatZazemiLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 20));
        PridatZazemiPanel pridatZazemiPanel = new PridatZazemiPanel();

        JLabel pridatOblastiLabel = new JLabel("Přidat Oblast");
        pridatOblastiLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 20));
        PridatOblastiPanel pridatOblastiPanel = new PridatOblastiPanel();

        JLabel pridatVybehuLabel = new JLabel("Přidat Výběh");
        pridatVybehuLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 20));
        PridatVybehuPanel pridatVybehuPanel = new PridatVybehuPanel();

        JButton goBackButton = new JButton("Zpět");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        add(goBackButton);
        c.fill = GridBagConstraints.BOTH;

        c.weightx = 1;

        c.gridy = 1;
        add(pridatZazemiLabel, c);
        c.gridx = 0;
        c.gridy = 2;
        add(pridatZazemiPanel, c);

        c.gridx = 1;
        c.gridy = 1;
        add(pridatOblastiLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(pridatOblastiPanel, c);

        c.gridx = 2;
        c.gridy = 1;
        add(pridatVybehuLabel, c);
        c.gridx = 2;
        c.gridy = 2;
        add(pridatVybehuPanel, c);

        c.weightx = 0;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 3;
        add(endLabel, c);

        goBackButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                setVisible(false);
                mainFrame.showRootPanel(user);
            }
        });
    }
}
