package gui.admin.pridat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import static model.Vybehy.getOblasti;
import static model.Vybehy.ulozZazemi;

public class PridatZazemiPanel extends JPanel {
    public PridatZazemiPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel oblastLabel = new JLabel("Oblast:");
        JComboBox<String> oblastCombo = new JComboBox<>(getOblasti());

        JLabel nazevLabel = new JLabel("Název :");
        JTextField nazevField = new JTextField();

        JLabel confirmLabel = new JLabel("");
        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(oblastLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(oblastCombo, c);
        oblastCombo.setSize(oblastCombo.getPreferredSize());

        c.gridx = 0;
        c.gridy = 1;
        add(nazevLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(nazevField, c);

        c.gridx = 0;
        c.gridy = 2;
        add(confirmLabel, c);

        c.gridx = 1;
        c.gridy = 2;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 3;
        add(endLabel, c);

        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                confirmLabel.setText(ulozZazemi(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString(), nazevField.getText()));
            }
        });
    }
}
