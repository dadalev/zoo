package gui.admin.pridat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static model.Vybehy.ulozOblast;

public class PridatOblastiPanel extends JPanel {
    public PridatOblastiPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel nazevLabel = new JLabel("Nazev: ");
        JTextField nazevField = new JTextField();

        JLabel polohaLabel = new JLabel("Poloha :");
        JTextField polohaField = new JTextField();

        JLabel confirmLabel = new JLabel("");
        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(nazevLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(nazevField, c);

        c.gridx = 0;
        c.gridy = 1;
        add(polohaLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(polohaField, c);

        c.gridx = 0;
        c.gridy = 2;
        add(confirmLabel, c);

        c.gridx = 1;
        c.gridy = 2;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 3;
        add(endLabel, c);


        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                confirmLabel.setText(ulozOblast(nazevField.getText(), polohaField.getText()));
            }
        });
    }
}
