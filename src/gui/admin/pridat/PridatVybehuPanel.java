package gui.admin.pridat;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import static model.Vybehy.*;

public class PridatVybehuPanel extends JPanel {
    public PridatVybehuPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel oblastLabel = new JLabel("Oblast:");
        JComboBox<String> oblastCombo = new JComboBox<>(getOblasti());

        JLabel nazevLabel = new JLabel("Nazev :");
        JTextField nazevField = new JTextField();

        JLabel typLabel = new JLabel("Typ výběhu :");
        JTextField typField = new JTextField();

        JLabel rozlohaLabel = new JLabel("Rozloha :");
        JTextField rozlohaField = new JTextField();

        JLabel maxPocetLabel = new JLabel("Maxinálí počet kusů :");
        JTextField maxPocetField = new JTextField();

        JLabel confirmLabel = new JLabel("");
        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(oblastLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(oblastCombo, c);
        oblastCombo.setSize(oblastCombo.getPreferredSize());

        c.gridx = 0;
        c.gridy = 1;
        add(nazevLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(nazevField, c);

        c.gridx = 0;
        c.gridy = 2;
        add(typLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(typField, c);

        c.gridx = 0;
        c.gridy = 3;
        add(rozlohaLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        add(rozlohaField, c);

        c.gridx = 0;
        c.gridy = 4;
        add(maxPocetLabel, c);
        c.gridx = 1;
        c.gridy = 4;
        add(maxPocetField, c);

        c.gridx = 0;
        c.gridy = 5;
        add(confirmLabel, c);
        c.gridx = 1;
        c.gridy = 5;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 6;
        add(endLabel, c);

        oblastCombo.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuCanceled(PopupMenuEvent e) {}
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e){}
            public void popupMenuWillBecomeVisible(PopupMenuEvent e)
            {
                DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(getOblasti());
                oblastCombo.setModel(model);
            }
        });

        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                confirmLabel.setText(ulozVybeh(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString(), nazevField.getText(), typField.getText(), rozlohaField.getText(), maxPocetField.getText()));
            }
        });
    }
}
