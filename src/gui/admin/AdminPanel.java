package gui.admin;

import gui.MainFrame;
import gui.admin.pridat.PridatPanel;
import gui.util.LogOutPanel;
import gui.util.LoggedAsPanel;
import gui.util.SwapPanel;
import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdminPanel extends JPanel {
    private final NewUserOutPanel newUserOutPanel;
    private final PridaniZamestnancePanel pridaniZamestnancePanel;

    public NewUserOutPanel getNewUserOutPanel() {
        return newUserOutPanel;
    }

    public JButton getMainButton() {
        return pridaniZamestnancePanel.getMainButton();
    }

    public AdminPanel(MainFrame mainFrame, User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        LogOutPanel logOutPanel = new LogOutPanel(mainFrame, this);
        JLabel titleLabel = new JLabel("Admin");
        titleLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 30));
        LoggedAsPanel loggedAsPanel = new LoggedAsPanel(mainFrame, this, user);
        SwapPanel swapPanel = new SwapPanel(mainFrame, this, user);

        newUserOutPanel = new NewUserOutPanel();
        pridaniZamestnancePanel = new PridaniZamestnancePanel(newUserOutPanel);
        ResetHeslaPanel resetPanel = new ResetHeslaPanel(mainFrame);
        JButton upravyButton = new JButton("Úpravy Rozložení Zoo");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(logOutPanel, c);

        c.gridx = 1;
        c.gridy = 0;
        add(titleLabel, c);

        c.gridx = 2;
        c.gridy = 0;
        add(loggedAsPanel, c);

        c.gridx = 2;
        c.gridy = 1;
        add(swapPanel, c);

        c.gridx = 0;
        c.gridy = 2;
        add(newUserOutPanel, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 3;
        add(upravyButton, c);
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weighty = 0;

        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 1;
        add(pridaniZamestnancePanel, c);

        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(resetPanel, c);

        upravyButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                PridatPanel pridatPanel = new PridatPanel(mainFrame, user);

                setVisible(false);

                pridatPanel.setVisible(true);
                mainFrame.getContentPane().add(pridatPanel);
            }
        });
    }
}
