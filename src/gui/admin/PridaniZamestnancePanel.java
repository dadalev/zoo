package gui.admin;

import gui.util.SpringUtilities;

import javax.swing.*;

import java.util.Objects;

import static model.Zamestnanec.getZazemi;
import static model.Zamestnanec.newZam;

public class PridaniZamestnancePanel extends JPanel {
    private final JTextField jmenoField = new JTextField();

    private final JTextField prijmeniField = new JTextField();

    private final JTextField rcField = new JTextField();

    private final JComboBox<String> pwTypeCombo = new JComboBox<>(new String[]{"123456(první písmeno ze Jména a Prijmení)", "6 náhodných čísel(první písmeno ze Jména a Prijmení)", "8 náhodných čísel"});

    private final JComboBox<String> roleCombo = new JComboBox<>(new String[]{"Údržbář", "Ošetřovatel", "Veterinář", "Kurátor", "Admin"});

    private final JComboBox<String> zazemiCombo = new JComboBox<>(getZazemi());

    private final JButton submitButton = new JButton("Přidat");

    public String getJmenoFieldText() {
        return jmenoField.getText();
    }

    public String getPrijmeniFieldText() {
        return prijmeniField.getText();
    }

    public int getRcFieldCislo() {
        return Integer.parseInt(rcField.getText());
    }

    public int getPwTypeComboIndex() {
        return pwTypeCombo.getSelectedIndex();
    }

    public JButton getMainButton() {
        return submitButton;
    }

    public PridaniZamestnancePanel(NewUserOutPanel newUserOutPanel) {
        super(new SpringLayout(), false);

        JLabel jmenoLabel = new JLabel("Jméno:");
        add(jmenoLabel);
        add(jmenoField);

        JLabel prijmeniLabel = new JLabel("Prijmení");
        add(prijmeniLabel);
        add(prijmeniField);

        JLabel rcLabel = new JLabel("Rodné Číslo:");
        add(rcLabel);
        add(rcField);

        JLabel pwTypeLabel = new JLabel("Typ hesla:");
        add(pwTypeLabel);
        add(pwTypeCombo);

        JLabel roleLabel = new JLabel("Role:");
        add(roleLabel);
        add(roleCombo);

        JLabel zazemiLabel = new JLabel("Zázemí:");
        add(zazemiLabel);
        add(zazemiCombo);

        JLabel emptyLabel = new JLabel("");
        add(emptyLabel);
        add(submitButton);

        SpringUtilities.makeCompactGrid(this,
                7, 2,  //rows, cols
                6, 6,  //initX, initY
                6, 6); //xPad, yPad

        submitButton.addActionListener(e -> {
                String[] userData = newZam(getJmenoFieldText(), getPrijmeniFieldText(), getRcFieldCislo(), getPwTypeComboIndex(), roleCombo.getSelectedIndex(), Objects.requireNonNull(zazemiCombo.getSelectedItem()).toString());
                newUserOutPanel.setYourLogin(userData);
        });
    }
}
