package gui.admin;

import gui.util.SpringUtilities;

import javax.swing.*;

public class NewUserOutPanel extends JPanel {
    private final JLabel yourLogin;
    private final JLabel yourPw;

    public void setYourLogin(String userLogin, String newPw) {
        setYourLogin(new String[]{userLogin, newPw});
    }

    public void setYourLogin(String[] userData) {
        yourLogin.setText("Login: " + userData[0]);
        yourPw.setText("Heslo: " + userData[1]);
    }

    public NewUserOutPanel() {
        super(new SpringLayout(),false);

        yourLogin = new JLabel("Login: ");
        yourPw = new JLabel("Heslo: ");

        add(yourLogin);
        add(yourPw);
        SpringUtilities.makeCompactGrid(this,
                2, 1,  //rows, cols
                6, 6,  //initX, initY
                6, 6);
    }
}
