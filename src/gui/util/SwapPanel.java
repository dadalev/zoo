package gui.util;

import gui.MainFrame;
import util.User;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SwapPanel extends JPanel {
    public SwapPanel(MainFrame mainFrame, JPanel parent, User user) {
        super(new SpringLayout(), false);

        JLabel spacerLabel = new JLabel(" ");

        JButton vratitButton = new JButton("Domů");
        JButton udrzbarButton = new JButton("Přejít na Údržbář");
        JButton osetrovatelButton = new JButton("Přejít na Ošetřovatel");
        JButton veterinarButton = new JButton("Přejít na Veterinar");
        JButton kuratorButton = new JButton("Přejít na Kurátor");

        int componentCount = 0;

        if(user.getId() == 3) {
            if(user.getPage() == 3) {
                add(osetrovatelButton);
                componentCount ++;
            } else if(user.getPage() == 1) {
                add(vratitButton);
                componentCount ++;
            }
        } else if(user.getId() == 4) {
            if(user.getPage() == 4) {
                add(udrzbarButton);
                add(veterinarButton);
                add(kuratorButton);
                componentCount += 3;
            } else if(user.getPage() == 3) {
                add(osetrovatelButton);
                add(spacerLabel);
                add(vratitButton);
                componentCount += 3;
            } else if(user.getPage() == 1) {
                add(kuratorButton);
                componentCount ++;
            } else {
                add(vratitButton);
                componentCount ++;
            }
        }

        SpringUtilities.makeCompactGrid(this,
                componentCount, 1,  //rows, cols
                6, 6,  //initX, initY
                6, 6);

        udrzbarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(user.getPage() != 0) {
                    user.setPage(0);
                    parent.setVisible(false);
                    mainFrame.remove(parent);
                    mainFrame.switchPanel(user);
                }
            }
        });

        osetrovatelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(user.getPage() != 1) {
                    user.setPage(1);
                    parent.setVisible(false);
                    mainFrame.remove(parent);
                    mainFrame.switchPanel(user);
                }
            }
        });

        veterinarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(user.getPage() != 2) {
                    user.setPage(2);
                    parent.setVisible(false);
                    mainFrame.remove(parent);
                    mainFrame.switchPanel(user);
                }
            }
        });

        kuratorButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(user.getPage() != 3) {
                    user.setPage(3);
                    parent.setVisible(false);
                    mainFrame.remove(parent);
                    mainFrame.switchPanel(user);
                }
            }
        });

        vratitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                user.setPage(user.getId());
                parent.setVisible(false);
                mainFrame.remove(parent);
                mainFrame.switchPanel(user);
            }
        });
    }
}