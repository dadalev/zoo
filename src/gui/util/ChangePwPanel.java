package gui.util;

import gui.MainFrame;
import util.User;

import javax.swing.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static model.Zamestnanec.changePw;

public class ChangePwPanel extends JPanel {
    private User user;
    private final JLabel loginLabel2;
    private final JPasswordField oldPwField;
    private final JPasswordField newPwField;
    private final JPasswordField newPwAgainField;
    private final JLabel changedPwConfirmLabel;

    public void setChangeLoginLabelText(User user) {
        this.user = user;
        loginLabel2.setText(user.getLogin());
    }

    public void changePassword(User user){
        changedPwConfirmLabel.setText(changePw(user, oldPwField.getPassword(), newPwField.getPassword(), newPwAgainField.getPassword()));
    }

    public ChangePwPanel(MainFrame mainFrame) {
        super(new SpringLayout(), false);

        JButton goBackButton = new JButton("Zpět");
        JLabel emptyLabel = new JLabel("");

        JLabel loginLabel1 = new JLabel("Login: ");
        loginLabel2 = new JLabel("none");

        JLabel oldPwLabel = new JLabel("Heslo: ");
        oldPwField = new JPasswordField(10);
        oldPwField.setEchoChar('*');

        JLabel newPwLabel = new JLabel("Nové Heslo: ");
        newPwField = new JPasswordField(10);
        newPwField.setEchoChar('*');

        JLabel newPwAgainLabel = new JLabel("Nové Heslo Znovu: ");
        newPwAgainField = new JPasswordField(10);
        newPwAgainField.setEchoChar('*');

        changedPwConfirmLabel = new JLabel("");
        JButton changePwNowButton = new JButton("Změnit Heslo");

        add(goBackButton);
        add(emptyLabel);

        add(loginLabel1);
        add(loginLabel2);

        add(oldPwLabel);
        add(oldPwField);
        oldPwField.setMaximumSize(newPwField.getPreferredSize());

        add(newPwLabel);
        add(newPwField);
        newPwField.setMaximumSize(newPwField.getPreferredSize());

        add(newPwAgainLabel);
        add(newPwAgainField);
        newPwAgainField.setMaximumSize(newPwField.getPreferredSize());

        add(changedPwConfirmLabel);
        add(changePwNowButton);

        SpringUtilities.makeCompactGrid(this,
                6, 2,  //rows, cols
                6, 6,  //initX, initY
                6, 6); //xPad, yPad

        changePwNowButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                changePassword(user);
            }
        });

        goBackButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                setVisible(false);
                mainFrame.showRootPanel(user);
            }
        });
    }
}
