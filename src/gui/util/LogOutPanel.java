package gui.util;

import gui.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LogOutPanel extends JPanel {
    public LogOutPanel(MainFrame mainFrame, JPanel parent) {
        super(new GridBagLayout(),false);

        JButton logOutButton = new JButton("Odhlásit");

        add(logOutButton);

        logOutButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                parent.setVisible(false);
                mainFrame.remove(parent);
                mainFrame.getLoginPanel().setVisible(true);
                mainFrame.getContentPane().add(mainFrame.getLoginPanel());
                mainFrame.setLoginButtonAsDefault();
            }
        });
    }
}
