package gui.util;

import gui.MainFrame;
import util.User;

import javax.swing.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LoggedAsPanel extends JPanel {
    public LoggedAsPanel(MainFrame mainFrame, JPanel parent, User user) {
        super(new SpringLayout(), false);
        ChangePwPanel changePwPanel = new ChangePwPanel(mainFrame);

        JLabel loggedInLabel = new JLabel("Login: " + user.getLogin());
        JLabel loggedInRoleLabel = new JLabel("Role:   " + user.getPosition());
        JButton changePwButton = new JButton("Změnit Heslo");

        add(loggedInLabel);
        add(loggedInRoleLabel);
        add(changePwButton);

        SpringUtilities.makeCompactGrid(this,
                3, 1,  //rows, cols
                6, 6,  //initX, initY
                6, 6);

        changePwButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                changePwPanel.setChangeLoginLabelText(user);

                parent.setVisible(false);

                changePwPanel.setVisible(true);
                mainFrame.getContentPane().add(changePwPanel);
            }
        });
    }
}
