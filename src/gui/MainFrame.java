package gui;

import javax.swing.*;
import java.awt.*;

import gui.admin.AdminPanel;
import gui.kurator.KuratorPanel;
import gui.login.LoginPanel;
import gui.osetrovatel.OsetrovatelPanel;
import gui.udrzbar.UdrzbarPanel;
import gui.veterinar.VeterinarPanel;
import util.*;

public class MainFrame extends JFrame {
    private final LoginPanel loginPanel;

    private UdrzbarPanel udrzbarPanel;
    private OsetrovatelPanel osetrovatelPanel;
    private VeterinarPanel veterinarPanel;
    private KuratorPanel kuratorPanel;
    private AdminPanel adminPanel;

    public MainFrame() {//inicializace JFrame
        setTitle("zoo");
        setSize(1600, 800);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new BorderLayout());

        loginPanel = new LoginPanel(this);//inicializace přihlašovacího okna
        add(loginPanel);

        setLoginButtonAsDefault();
    }

    public UdrzbarPanel getUdrzbarPanel() {
        return udrzbarPanel;
    }
    public OsetrovatelPanel getOsetrovatelPanel() {
        return osetrovatelPanel;
    }
    public VeterinarPanel getVeterinarPanel() {
        return veterinarPanel;
    }
    public KuratorPanel getKuratorPanel() {
        return kuratorPanel;
    }
    public AdminPanel getAdminPanel() {
        return adminPanel;
    }

    public JPanel getLoginPanel() {
        return loginPanel;
    }

    public void setLoginButtonAsDefault() {
        getRootPane().setDefaultButton(loginPanel.getMainButton());
    }

    public void showRootPanel(User user) {//vrácení do domovského okna
        if(user.getPage() == 0) {
            udrzbarPanel.setVisible(true);
            getContentPane().add(udrzbarPanel);
        }
        else if(user.getPage() == 1) {
            osetrovatelPanel.setVisible(true);
            getContentPane().add(osetrovatelPanel);
        }
        else if(user.getPage() == 2) {
            veterinarPanel.setVisible(true);
            getContentPane().add(veterinarPanel);
        }
        else if(user.getPage() == 3) {
            kuratorPanel.setVisible(true);
            getContentPane().add(kuratorPanel);
        }
        else if(user.getPage() == 4) {
            adminPanel.setVisible(true);
            getContentPane().add(adminPanel);
        }
        getContentPane().repaint();
    }

    public void switchPanel(User user) {//přepínání mezi uživateli
        if(user.getPage() == 0) {
            udrzbarPanel = new UdrzbarPanel(this, user);
            getRootPane().setDefaultButton(udrzbarPanel.getMainButton());
            getUdrzbarPanel().setVisible(true);
            getContentPane().add(getUdrzbarPanel());
        }
        else if(user.getPage() == 1) {
            osetrovatelPanel = new OsetrovatelPanel(this, user);
            getRootPane().setDefaultButton(osetrovatelPanel.getMainButton());
            getOsetrovatelPanel().setVisible(true);
            getContentPane().add(getOsetrovatelPanel());
        }
        else if(user.getPage() == 2) {
            veterinarPanel  = new VeterinarPanel(this, user);
            getRootPane().setDefaultButton(veterinarPanel.getMainButton());
            getVeterinarPanel().setVisible(true);
            getContentPane().add(getVeterinarPanel());
        }
        else if(user.getPage() == 3) {
            kuratorPanel = new KuratorPanel(this, user);
            getRootPane().setDefaultButton(kuratorPanel.getMainButton());
            getKuratorPanel().setVisible(true);
            getContentPane().add(getKuratorPanel());
        }
        else if(user.getPage() == 4) {
            adminPanel = new AdminPanel(this, user);
            getRootPane().setDefaultButton(adminPanel.getMainButton());
            getAdminPanel().setVisible(true);
            getContentPane().add(getAdminPanel());
        }
        getContentPane().repaint();
    }
}
