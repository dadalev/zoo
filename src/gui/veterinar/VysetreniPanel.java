package gui.veterinar;

import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Objects;

import static model.Zvire.*;

public class VysetreniPanel extends JPanel{
    private File[] files;
    private final JComboBox<String> druhComboBox;
    private final JButton ulozButton;

    public JButton getMainButton() {
        return ulozButton;
    }

    public VysetreniPanel(User user) {
        super(new GridBagLayout(), false);
        GridBagConstraints c = new GridBagConstraints();

        JLabel druhLabel = new JLabel("Druh: ");
        String[] druhy = getDruhy();
        druhComboBox = new JComboBox<>(druhy);

        JLabel jmenoLabel = new JLabel("Jméno :");
        String[] jmena = getJmena(-1);
        JComboBox<String> jmenoComboBox = new JComboBox<>(jmena);

        JLabel datumLabel = new JLabel("Datum :");
        JTextField datumField = new JTextField("dnes");

        JLabel priznakyLabel = new JLabel("Příznaky :");
        JTextField priznakyField = new JTextField();

        JLabel nalezLabel = new JLabel("Nález :");
        JTextField nalezField = new JTextField();

        JLabel lecbaLabel = new JLabel("Léčba :");
        JTextField lecbaField = new JTextField();

        JLabel rentgenLabel = new JLabel("Rentgen :");
        JButton rentgenButton = new JButton("Vybrat Rentgen");

        JLabel confirmLabel = new JLabel("");
        ulozButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");
        JLabel RightSpacerLabel = new JLabel("");
        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(druhLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(druhComboBox, c);
        druhComboBox.setSize(druhComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 1;
        add(jmenoLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(jmenoComboBox, c);
        jmenoComboBox.setSize(jmenoComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 2;
        add(datumLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(datumField, c);
        datumField.setSize(datumField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 3;
        add(priznakyLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        add(priznakyField, c);
        priznakyField.setSize(priznakyField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 4;
        add(nalezLabel, c);
        c.gridx = 1;
        c.gridy = 4;
        add(nalezField, c);
        nalezField.setSize(nalezField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 5;
        add(lecbaLabel, c);
        c.gridx = 1;
        c.gridy = 5;
        add(lecbaField, c);
        lecbaField.setSize(lecbaField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 6;
        add(rentgenLabel, c);
        c.gridx = 1;
        c.gridy = 6;
        add(rentgenButton, c);
        lecbaField.setSize(rentgenButton.getPreferredSize());

        c.gridx = 0;
        c.gridy = 7;
        add(confirmLabel, c);
        c.gridx = 1;
        c.gridy = 7;
        add(ulozButton, c);
        priznakyField.setSize(ulozButton.getPreferredSize());

        c.gridx = 3;
        c.gridy = 0;
        c.ipadx = 50;
        add(RightSpacerLabel, c);
        c.ipadx = 0;

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 8;
        add(endLabel, c);

        druhComboBox.addActionListener(e -> {
            String[] jmena1 = getJmena(getIdFromDruh(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString()));
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(jmena1);
            jmenoComboBox.setModel(model);
        });

        ulozButton.addActionListener(e ->
                confirmLabel.setText(ulozVysetreni(
                        datumField.getText(),
                        Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString(),
                        Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(),
                        priznakyField.getText(),
                        nalezField.getText(),
                        lecbaField.getText(),
                        user,
                        files)));

        rentgenButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                FileDialog dialog = new FileDialog((Frame) null, "Vyberte rentgen");
                dialog.setMode(FileDialog.LOAD);
                dialog.setMultipleMode(true);
                dialog.setVisible(true);
                files = dialog.getFiles();
            }
        });
    }
}
