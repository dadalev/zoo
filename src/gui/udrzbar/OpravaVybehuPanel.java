package gui.udrzbar;

import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import static model.Vybehy.*;
import static model.Zvire.*;

public class OpravaVybehuPanel extends JPanel {
    public OpravaVybehuPanel(User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel oblastLabel = new JLabel("Oblast:");
        JComboBox<String> oblastCombo = new JComboBox<>(getOblasti());

        JLabel vybehLabel = new JLabel("Výběh:");
        JComboBox<String> vybehCombo = new JComboBox<>(getVybehy(-1));

        JLabel popisLabel = new JLabel("Popis opravy/úpravy:");
        JTextField popisField = new JTextField();

        JLabel datumLabel = new JLabel("Datum:");
        JTextField datumField = new JTextField("dnes");

        JLabel confirmLabel = new JLabel("");
        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;


        c.gridx = 0;
        c.gridy = 0;
        add(oblastLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(oblastCombo, c);
        oblastCombo.setSize(oblastCombo.getPreferredSize());


        c.gridx = 0;
        c.gridy = 1;
        add(vybehLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(vybehCombo, c);
        vybehCombo.setSize(vybehCombo.getPreferredSize());

        c.gridx = 0;
        c.gridy = 2;
        add(popisLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(popisField, c);

        c.gridx = 0;
        c.gridy = 3;
        add(datumLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        add(datumField, c);

        c.gridx = 0;
        c.gridy = 4;
        add(confirmLabel, c);
        c.gridx = 1;
        c.gridy = 4;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 5;
        add(endLabel, c);

        oblastCombo.addActionListener(e -> {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(getVybehy(getIdFromOblast(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString())));
            vybehCombo.setModel(model);
        });

        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                confirmLabel.setText(ulozOpravaUpravaVybehu(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString(), Objects.requireNonNull(vybehCombo.getSelectedItem()).toString(), popisField.getText(), datumField.getText(), user));
            }
        });
    }
}
