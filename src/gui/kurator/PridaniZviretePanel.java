package gui.kurator;

import util.User;

import javax.swing.*;

import java.awt.*;
import java.util.Objects;

import static model.Vybehy.*;
import static model.Zvire.*;

public class PridaniZviretePanel  extends JPanel {
    private final JTextField druhField;
    private final JTextField jmenofield;
    private final JComboBox<String> pohlaviCombo;
    private final JTextField vahaField;
    private final JTextField datumPorizeniField;
    private final JTextField datumNarozeniField;
    private final JTextField puvodField;
    private final JTextField cisloPlemeneField;
    private final JComboBox<String> tridaCombo;
    private final JTextField celedField;
    private final JTextField radField;
    private final JTextField rodField;
    private final JComboBox<String> oblastCombo;
    private final JComboBox<String> vybehCombo;

    public JButton getMainButton() {
        return null;
    }

    public PridaniZviretePanel(User user) {
        super(new GridBagLayout(), false);
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;

        JLabel druhLabel = new JLabel("Druh:");
        c.gridx = 0;
        c.gridy = 0;
        add(druhLabel, c);
        druhField = new JTextField();
        c.gridx = 1;
        c.gridy = 0;
        add(druhField, c);

        JLabel jmenoLabel = new JLabel("Jméno");
        c.gridx = 0;
        c.gridy += 1;
        add(jmenoLabel, c);
        jmenofield = new JTextField();
        c.gridx = 1;
        add(jmenofield, c);

        JLabel pohlaviLabel = new JLabel("Pohlaví:");
        c.gridx = 0;
        c.gridy += 1;
        add(pohlaviLabel, c);
        pohlaviCombo = new JComboBox<>(new String[]{"Vyberte Pohlaví", "Samec", "Samice"});
        c.gridx = 1;
        add(pohlaviCombo, c);
        pohlaviCombo.setSize(pohlaviCombo.getPreferredSize());

        JLabel vahaLabel = new JLabel("Váha:");
        c.gridx = 0;
        c.gridy += 1;
        add(vahaLabel, c);
        vahaField = new JTextField();
        c.gridx = 1;
        add(vahaField, c);

        JLabel datumPorizeniLabel = new JLabel("Datum pořízení:");
        c.gridx = 0;
        c.gridy += 1;
        add(datumPorizeniLabel, c);
        datumPorizeniField = new JTextField("dnes");
        c.gridx = 1;
        add(datumPorizeniField, c);

        JLabel datumNarozeniLabel = new JLabel("Datum narození:");
        c.gridx = 0;
        c.gridy += 1;
        add(datumNarozeniLabel, c);
        datumNarozeniField = new JTextField("dnes");
        c.gridx = 1;
        add(datumNarozeniField, c);

        JLabel puvodLabel = new JLabel("Původ:");
        c.gridx = 0;
        c.gridy += 1;
        add(puvodLabel, c);
        puvodField = new JTextField();
        c.gridx = 1;
        add(puvodField, c);

        JLabel cisloPlemeneLabel = new JLabel("Číslo v plemené knize:");
        c.gridx = 0;
        c.gridy += 1;
        add(cisloPlemeneLabel, c);
        cisloPlemeneField = new JTextField();
        c.gridx = 1;
        add(cisloPlemeneField, c);

        JLabel tridaLabel = new JLabel("Třída:");
        c.gridx = 0;
        c.gridy += 1;
        add(tridaLabel, c);
        tridaCombo = new JComboBox<>(getTrida());
        c.gridx = 1;
        add(tridaCombo, c);
        tridaCombo.setSize(tridaCombo.getPreferredSize());

        JLabel celedLabel = new JLabel("Čeleď:");
        c.gridx = 0;
        c.gridy += 1;
        add(celedLabel, c);
        celedField = new JTextField();
        c.gridx = 1;
        add(celedField, c);

        JLabel radLabel = new JLabel("Řád:");
        c.gridx = 0;
        c.gridy += 1;
        add(radLabel, c);
        radField = new JTextField();
        c.gridx = 1;
        add(radField, c);

        JLabel rodLabel = new JLabel("Rod:");
        c.gridx = 0;
        c.gridy += 1;
        add(rodLabel, c);
        rodField = new JTextField();
        c.gridx = 1;
        add(rodField, c);

        JLabel oblastLabel = new JLabel("Oblast:");
        c.gridx = 0;
        c.gridy += 1;
        add(oblastLabel, c);
        oblastCombo = new JComboBox<>(getOblasti());
        c.gridx = 1;
        add(oblastCombo, c);
        oblastCombo.setSize(oblastCombo.getPreferredSize());

        JLabel vybehLabel = new JLabel("Výběh:");
        c.gridx = 0;
        c.gridy += 1;
        add(vybehLabel, c);
        vybehCombo = new JComboBox<>(getVybehy(-1));
        c.gridx = 1;
        add(vybehCombo, c);
        vybehCombo.setSize(vybehCombo.getPreferredSize());

        JLabel confirmLabel = new JLabel("");
        c.gridx = 0;
        c.gridy += 1;
        add(confirmLabel, c);
        JButton submitButton = new JButton("Přidat");
        c.gridx = 1;
        add(submitButton, c);

        oblastCombo.addActionListener(e -> {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(getVybehy(getIdFromOblast(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString())));
            vybehCombo.setModel(model);
        });

        submitButton.addActionListener(e ->
                confirmLabel.setText(newZvire(
                druhField.getText(),
                jmenofield.getText(),
                Objects.requireNonNull(pohlaviCombo.getSelectedItem()).toString(),
                vahaField.getText(),
                datumPorizeniField.getText(),
                datumNarozeniField.getText(),
                puvodField.getText(),
                cisloPlemeneField.getText(),
                Objects.requireNonNull(tridaCombo.getSelectedItem()).toString(),
                celedField.getText(),
                radField.getText(),
                rodField.getText(),
                Objects.requireNonNull(oblastCombo.getSelectedItem()).toString(),
                Objects.requireNonNull(vybehCombo.getSelectedItem()).toString(),
                user)
        ));
    }
}
