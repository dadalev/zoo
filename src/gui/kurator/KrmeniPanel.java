package gui.kurator;

import gui.MainFrame;
import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class KrmeniPanel extends JPanel {
    public KrmeniPanel(MainFrame mainFrame, User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        NoveKrmeniPanel noveKrmeniPanel = new NoveKrmeniPanel(user);
        ZadatRozpisKrmeniPanel rozpisKrmeniPanel = new ZadatRozpisKrmeniPanel(user);

        JButton goBackButton = new JButton("Zpět");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        add(goBackButton);
        c.fill = GridBagConstraints.BOTH;

        c.gridy = 1;
        add(noveKrmeniPanel, c);

        c.gridx = 1;
        c.gridy = 1;
        add(rozpisKrmeniPanel, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 2;
        add(endLabel, c);

        goBackButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                setVisible(false);
                mainFrame.showRootPanel(user);
            }
        });
    }
}
