package gui.kurator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import static model.Zamestnanec.getOsetrovatel;
import static model.Zvire.*;

public class PriraditOsetrovatelePanel extends JPanel {
    public PriraditOsetrovatelePanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel druhLabel = new JLabel("Druh: ");
        JComboBox<String> druhComboBox = new JComboBox<>(getDruhy());

        JLabel jmenoLabel = new JLabel("Jméno :");
        JComboBox<String> jmenoComboBox = new JComboBox<>(getJmena(-1));

        JLabel nazevKrmeniLabel = new JLabel("Ošetřovatel :");
        JComboBox<String> osetrovatelComboBox = new JComboBox<>(getOsetrovatel());

        JLabel confirmLabel = new JLabel("");

        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(druhLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(druhComboBox, c);
        druhComboBox.setSize(druhComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 1;
        add(jmenoLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(jmenoComboBox, c);
        jmenoComboBox.setSize(jmenoComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 2;
        add(nazevKrmeniLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(osetrovatelComboBox, c);
        osetrovatelComboBox.setSize(osetrovatelComboBox.getPreferredSize());

        c.gridx = 1;
        c.gridy = 3;
        add(confirmLabel, c);

        c.gridx = 2;
        c.gridy = 3;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 4;
        add(endLabel, c);

        druhComboBox.addActionListener(e -> {
            String[] jmena1 = getJmena(getIdFromDruh(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString()));
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(jmena1);
            jmenoComboBox.setModel(model);
        });

        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                confirmLabel.setText(priraditOsetrovatele(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(), Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString(), Objects.requireNonNull(osetrovatelComboBox.getSelectedItem()).toString()));
            }
        });
    }
}