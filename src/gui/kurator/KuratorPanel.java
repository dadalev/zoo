package gui.kurator;

import gui.MainFrame;
import gui.util.LogOutPanel;
import gui.util.LoggedAsPanel;
import gui.util.SwapPanel;
import table.InfoPanel;
import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class KuratorPanel extends JPanel {

    public JButton getMainButton() {
        return null;
    }

    public KuratorPanel(MainFrame mainFrame, User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        LogOutPanel logOutPanel = new LogOutPanel(mainFrame, this);
        JLabel titleLabel = new JLabel("Kurátor");
        titleLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 30));
        LoggedAsPanel loggedAsPanel = new LoggedAsPanel(mainFrame, this, user);
        SwapPanel swapPanel = new SwapPanel(mainFrame, this, user);

        PridaniZviretePanel pridaniZviretePanel = new PridaniZviretePanel(user);
        InfoPanel infoPanel = new InfoPanel(mainFrame, user);
        PriraditOsetrovatelePanel priraditOsetrovatelePanel = new PriraditOsetrovatelePanel();
        JButton krmeniButton = new JButton("Rozpis Krmení");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(logOutPanel, c);

        c.gridx = 1;
        c.gridy = 0;
        add(titleLabel, c);

        c.gridx = 3;
        c.gridy = 0;
        add(loggedAsPanel, c);

        c.gridx = 3;
        c.gridy = 1;
        add(swapPanel, c);

        c.gridx = 0;
        c.gridy = 1;
        add(pridaniZviretePanel, c);

        c.gridx = 2;
        c.gridy = 1;
        add(priraditOsetrovatelePanel, c);

        c.fill = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 2;
        add(krmeniButton, c);
        c.fill = GridBagConstraints.BOTH;

        c.weightx = 1;
        c.weighty = 1;
        c.gridheight = 2;
        c.gridx = 1;
        c.gridy = 1;
        add(infoPanel, c);

        krmeniButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                KrmeniPanel krmeniPanel = new KrmeniPanel(mainFrame, user);

                setVisible(false);

                krmeniPanel.setVisible(true);
                mainFrame.getContentPane().add(krmeniPanel);
            }
        });
    }
}
