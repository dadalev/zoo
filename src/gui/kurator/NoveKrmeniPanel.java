package gui.kurator;

import util.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import static model.Zvire.getPolozkyKrmeni;
import static model.Zvire.ulozKrmeni;

public class NoveKrmeniPanel extends JPanel {
    private final int componentOfSet;
    private int extraRows;

    public NoveKrmeniPanel(User user) {
        componentOfSet = 15;
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel nazevLabel = new JLabel("Název Krmení: ");
        JTextField nazevField = new JTextField("");

        JLabel prvniKrmeniLabel = new JLabel("Datum prvního krmení: ");
        JTextField prvniKrmeniField = new JTextField("dnes");

        JLabel jakCastoLabel = new JLabel("Jak často (dní): ");
        JTextField jakCastoField = new JTextField("");

        JLabel vKolikLabel = new JLabel("V kolik hodin: (5:00 - 22:00)");
        JTextField vKolikField = new JTextField("");

        JLabel pocetCastoLabel = new JLabel("Množství");

        JButton novaPolozkaButton = new JButton("+");

        JLabel confirmLabel = new JLabel("");

        JButton ulozitButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(nazevLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(nazevField, c);

        c.gridx = 0;
        c.gridy = 1;
        add(prvniKrmeniLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(prvniKrmeniField, c);

        c.gridx = 0;
        c.gridy = 2;
        add(jakCastoLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(jakCastoField, c);

        c.gridx = 0;
        c.gridy = 3;
        add(vKolikLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        add(vKolikField, c);

        c.gridx = 2;
        c.gridy = 4;
        add(pocetCastoLabel, c);

        c.gridx = 1;
        c.gridy = 15;
        add(novaPolozkaButton, c);

        c.gridx = 0;
        c.gridy = 16;
        add(confirmLabel, c);
        c.gridx = 2;
        c.gridy = 16;
        add(ulozitButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 17;
        add(endLabel, c);

        String[] polozkyKrmeni = getPolozkyKrmeni();
        JComboBox<String> novyCombo1 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 5;
        add(novyCombo1, c);
        JTextField novyField1 = new JTextField("");
        c.gridx = 2;
        add(novyField1, c);

        JComboBox<String> novyCombo2 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 6;
        add(novyCombo2, c);
        JTextField novyField2 = new JTextField("");
        c.gridx = 2;
        add(novyField2, c);

        JComboBox<String> novyCombo3 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 7;
        add(novyCombo3, c);
        JTextField novyField3 = new JTextField("");
        c.gridx = 2;
        add(novyField3, c);

        JComboBox<String> novyCombo4 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 8;
        add(novyCombo4, c);
        JTextField novyField4 = new JTextField("");
        c.gridx = 2;
        add(novyField4, c);

        JComboBox<String> novyCombo5 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 9;
        add(novyCombo5, c);
        JTextField novyField5 = new JTextField("");
        c.gridx = 2;
        add(novyField5, c);

        JComboBox<String> novyCombo6 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 10;
        add(novyCombo6, c);
        JTextField novyField6 = new JTextField("");
        c.gridx = 2;
        add(novyField6, c);

        JComboBox<String> novyCombo7= new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 11;
        add(novyCombo7, c);
        JTextField novyField7 = new JTextField("");
        c.gridx = 2;
        add(novyField7, c);

        JComboBox<String> novyCombo8 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 12;
        add(novyCombo8, c);
        JTextField novyField8 = new JTextField("");
        c.gridx = 2;
        add(novyField8, c);

        JComboBox<String> novyCombo9 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 13;
        add(novyCombo9, c);
        JTextField novyField9 = new JTextField("");
        c.gridx = 2;
        add(novyField9, c);

        JComboBox<String> novyCombo10 = new JComboBox<>(polozkyKrmeni);
        c.gridx = 1;
        c.gridy = 14;
        add(novyCombo10, c);
        JTextField novyField10 = new JTextField("");
        c.gridx = 2;
        add(novyField10, c);

        for(int i = componentOfSet; i <= componentOfSet+17; i++) {
            getComponent(i).setVisible(false);
        }

        novaPolozkaButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if(componentOfSet + extraRows < 22) {
                    getComponent(componentOfSet + extraRows*2).setVisible(true);
                    getComponent(componentOfSet + extraRows*2 +1).setVisible(true);
                    extraRows ++;
                }
            }
        });
        ulozitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                String[][] obsahKrmeni = new String[0][2];
                switch (extraRows) {
                    case 9:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo10.getSelectedItem()).toString(), novyField10.getText()});
                    case 8:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo9.getSelectedItem()).toString(), novyField9.getText()});
                    case 7:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo8.getSelectedItem()).toString(), novyField8.getText()});
                    case 6:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo7.getSelectedItem()).toString(), novyField7.getText()});
                    case 5:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo6.getSelectedItem()).toString(), novyField6.getText()});
                    case 4:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo5.getSelectedItem()).toString(), novyField5.getText()});
                    case 3:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo4.getSelectedItem()).toString(), novyField4.getText()});
                    case 2:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo3.getSelectedItem()).toString(), novyField3.getText()});
                    case 1:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo2.getSelectedItem()).toString(), novyField2.getText()});
                    case 0:
                        obsahKrmeni = addRowToString2D(obsahKrmeni, new String[] {Objects.requireNonNull(novyCombo1.getSelectedItem()).toString(), novyField1.getText()});
                }
                confirmLabel.setText(ulozKrmeni(
                        nazevField.getText(),
                        prvniKrmeniField.getText(),
                        jakCastoField.getText(),
                        vKolikField.getText(), obsahKrmeni,
                        user));
            }
        });
    }

    private static String[][] addRowToString2D(String[][] string2D, String[] row) {
        String[][] temp = string2D.clone();
        string2D = new String[string2D.length + 1][2];
        System.arraycopy(temp, 0, string2D, 0, temp.length);
        string2D[string2D.length-1] = row;
        return string2D;
    }
}
