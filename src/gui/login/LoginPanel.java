package gui.login;

import gui.MainFrame;
import gui.util.SpringUtilities;
import util.User;

import javax.swing.*;

import static model.LogIn.loginAndGetUser;

public class LoginPanel extends JPanel {
    private final JTextField loginField = new JTextField(10);

    private final JPasswordField pwField = new JPasswordField(10);

    private final JLabel statusLabel = new JLabel("");
    private final JButton logInButton = new JButton("Přihlásit");

    public JButton getMainButton() {
        return logInButton;
    }

    public String getLoginFieldText() {
        return loginField.getText();
    }

    public char[] getPwFieldPw() {
        return pwField.getPassword();
    }

    public LoginPanel(MainFrame mainframe) {
        super(new SpringLayout(), false);

        JLabel loginLabel = new JLabel("Login:");
        add(loginLabel);
        add(loginField);
        loginField.setMaximumSize(loginField.getPreferredSize());


        JLabel pwLabel = new JLabel("Password:");
        add(pwLabel);
        add(pwField);
        pwField.setEchoChar('*');
        pwField.setMaximumSize(pwField.getPreferredSize());

        add(statusLabel);
        add(logInButton);

        loginField.setText("veter1");
        String pw = "veterinar1";
        // udrzb1 udrzbar1
        // osetr1 osetrovatel1
        // veter1 veterinar1
        // kurat1 kurator1
        // admin1 admin1
        pwField.setText(pw);

        SpringUtilities.makeCompactGrid(this,
                3, 2,  //rows, cols
                6, 6,  //initX, initY
                6, 6); //xPad, yPad

        logInButton.addActionListener(e -> {

            User user = loginAndGetUser(getLoginFieldText(), getPwFieldPw());
            if(user.getId() != -1){
                pwField.setText(pw);
                statusLabel.setText("");
                setVisible(false);
                mainframe.switchPanel(user);
            } else {
                statusLabel.setText(user.getPosition());
            }
        });
    }
}
