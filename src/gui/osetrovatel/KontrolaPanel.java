package gui.osetrovatel;

import util.User;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

import static model.Zvire.*;

public class KontrolaPanel extends JPanel{
    private final JComboBox<String> druhComboBox;
    private final JTextField datumField;
    private final JTextField vahaField;
    private final JTextField potizeField;
    private final JButton ulozButton;

    public JButton getMainButton() {
        return ulozButton;
    }

    public KontrolaPanel(User user) {
        super(new GridBagLayout(), false);
        GridBagConstraints c = new GridBagConstraints();

        JLabel druhLabel = new JLabel("Druh: ");
        String[] druhy = getDruhy();
        druhComboBox = new JComboBox<>(druhy);

        JLabel jmenoLabel = new JLabel("Jméno :");
        String[] jmena = getJmena(-1);
        JComboBox<String> jmenoComboBox = new JComboBox<>(jmena);

        JLabel datumLabel = new JLabel("Datum :");
        datumField = new JTextField("dnes");

        JLabel vahaLabel = new JLabel("Váha :");
        vahaField = new JTextField();
        JLabel kgLabel = new JLabel("Kg");

        JLabel potizeLabel = new JLabel("Potíže :");
        potizeField = new JTextField();

        JLabel confirmLabel = new JLabel("");
        ulozButton = new JButton("Uložit");

        JLabel endLabel = new JLabel("");
        JLabel RightSpacerLabel = new JLabel("");
        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(druhLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(druhComboBox, c);
        druhComboBox.setSize(druhComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 1;
        add(jmenoLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        add(jmenoComboBox, c);
        jmenoComboBox.setSize(jmenoComboBox.getPreferredSize());

        c.gridx = 0;
        c.gridy = 2;
        add(datumLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        add(datumField, c);
        datumField.setSize(datumField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 3;
        add(vahaLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        add(vahaField, c);
        vahaField.setSize(vahaField.getPreferredSize());
        c.gridx = 2;
        c.gridy = 3;
        add(kgLabel, c);

        c.gridx = 0;
        c.gridy = 4;
        add(potizeLabel, c);
        c.gridx = 1;
        c.gridy = 4;
        add(potizeField, c);
        potizeField.setSize(potizeField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 5;
        add(confirmLabel, c);
        c.gridx = 1;
        c.gridy = 5;
        add(ulozButton, c);
        potizeField.setSize(ulozButton.getPreferredSize());

        c.gridx = 4;
        c.gridy = 0;
        c.ipadx = 50;
        add(RightSpacerLabel, c);
        c.ipadx = 0;

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 6;
        add(endLabel, c);

        druhComboBox.addActionListener(e -> {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(getJmena(getIdFromDruh(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString())));
            jmenoComboBox.setModel(model);
        });

        ulozButton.addActionListener(e ->
                confirmLabel.setText(ulozKontrola(
                        datumField.getText(),
                        Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString(),
                        Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(),
                        potizeField.getText(),
                        vahaField.getText(),
                        user)));
    }
}