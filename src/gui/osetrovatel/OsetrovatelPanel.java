package gui.osetrovatel;

import gui.MainFrame;
import gui.util.LogOutPanel;
import gui.util.LoggedAsPanel;
import gui.util.SwapPanel;
import table.InfoPanel;
import util.User;

import javax.swing.*;
import java.awt.*;

public class OsetrovatelPanel extends JPanel {
    private final KontrolaPanel kontrolaPanel;

    public JButton getMainButton() {
        return kontrolaPanel.getMainButton();
    }

    public OsetrovatelPanel(MainFrame mainFrame, User user) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        LogOutPanel logOutPanel = new LogOutPanel(mainFrame, this);
        JLabel titleLabel = new JLabel("Ošetřovatel");
        titleLabel.setFont(new Font("MS Sans Serif", Font.BOLD, 30));
        LoggedAsPanel loggedAsPanel = new LoggedAsPanel(mainFrame, this, user);
        SwapPanel swapPanel = new SwapPanel(mainFrame, this, user);

        kontrolaPanel = new KontrolaPanel(user);
        InfoPanel zvirataInfoPanel = new InfoPanel(mainFrame, user);

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        add(logOutPanel, c);

        c.gridx = 1;
        c.gridy = 0;
        add(titleLabel, c);

        c.gridx = 2;
        c.gridy = 0;
        add(loggedAsPanel, c);

        c.gridx = 2;
        c.gridy = 1;
        add(swapPanel, c);

        c.gridx = 0;
        c.gridy = 1;
        add(kontrolaPanel, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(zvirataInfoPanel, c);
    }
}
