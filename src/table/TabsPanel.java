package table;

import gui.MainFrame;
import table.tabs.*;
import util.User;

import javax.swing.*;

public class TabsPanel extends JTabbedPane {
    private final VysetreniHistoriePanel vysetreniHistoriePanel;
    private final KontrolaHistoriePanel kontrolaHistoriePanel;
    private final VybehyHistoriePanel vybehyHistoriePanel;

    public void setDataVysetreni(String[][] data, String druh, String jmeno) {
        vysetreniHistoriePanel.setDtm(data, druh, jmeno);
    }

    public void setDtmKontrola(String[][] data) {
        kontrolaHistoriePanel.setDtm(data);
    }

    public void setDtmVybehy(String[][] data) {
        vybehyHistoriePanel.setDtm(data);
    }

    public TabsPanel(MainFrame mainFrame, User user) {
        vysetreniHistoriePanel = new VysetreniHistoriePanel(mainFrame);
        kontrolaHistoriePanel = new KontrolaHistoriePanel();
        PocetMladatPanel pocetMladatPanel = new PocetMladatPanel();
        PocetSamcuSamicPanel pocetSamcuSamicPanel = new PocetSamcuSamicPanel();
        ObsazenostVybehuPanel obsazenostVybehuPanel = new ObsazenostVybehuPanel();
        RozvrhKrmeniPanel rozvrhKrmeniPanel = new RozvrhKrmeniPanel(mainFrame, user);
        vybehyHistoriePanel = new VybehyHistoriePanel();

        ImageIcon icon = new ImageIcon();

        if(user.getPage() == 0) {
            addTab("Výběhy", icon, vybehyHistoriePanel);
        }
        if(user.getPage() == 1) {
            addTab("Kontroly", icon, kontrolaHistoriePanel);
            addTab("Rozpis Krmení", icon, rozvrhKrmeniPanel);
        }
        if(user.getPage() == 2) {
            addTab("Vysetreni", icon, vysetreniHistoriePanel);
            addTab("Kontroly", icon, kontrolaHistoriePanel);
        }
        if(user.getPage() == 3) {
            addTab("Vysetreni", icon, vysetreniHistoriePanel);
            addTab("Kontroly", icon, kontrolaHistoriePanel);
            addTab("Počet mláďat", icon, pocetMladatPanel);
            addTab("Počet Samců Samic", icon, pocetSamcuSamicPanel);
            addTab("Obsazenost výběhu", icon, obsazenostVybehuPanel);
        }
    }
}
