package table.tabs;

import gui.MainFrame;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

import static model.Zvire.loadRentgen;

public class VysetreniHistoriePanel extends JPanel {
    public int[] vysetreniID = new int[0];
    public JTable vypisTable;
    public DefaultTableModel dtm;
    public  String druh, jmeno;
    private final JFrame frame;

    public void setDtm(String[][] dataIn, String druh, String jmeno) {
        this.druh = druh;
        this.jmeno = jmeno;
        this.vysetreniID = new int[0];
        for (String[] strings : dataIn) {
            int[] temp = vysetreniID.clone();
            vysetreniID = new int[vysetreniID.length + 1];
            System.arraycopy(temp, 0, vysetreniID, 0, temp.length);
            vysetreniID[vysetreniID.length - 1] = Integer.parseInt(strings[7]);
        }
        String[][] data = new String[dataIn.length][7];
        for(int i = 0; i < dataIn.length; i++) {
            System.arraycopy(dataIn[i], 0, data[i], 0, 7);
        }

        dtm.setDataVector(data, new String[]{"Datum", "Druh", "Jméno", "Příznaky", "Nález", "Léčba", "snímky"});

        vypisTable.getColumnModel().getColumn(6).setCellRenderer(new ClientsTableButtonRenderer());
        vypisTable.getColumnModel().getColumn(6).setCellEditor(new ClientsTableRenderer(new JCheckBox()));
    }

    public VysetreniHistoriePanel(MainFrame mainFrame) {
        super(new BorderLayout(), false);
        frame = mainFrame;

        String[] columns = {"Datum", "Druh", "Jméno", "Příznaky", "Nález", "Léčba", "snímky"};

        dtm = new DefaultTableModel(null, columns)
        {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column)
            {
                return column == 6;
            }
        };
        dtm.setColumnIdentifiers(columns);
        vypisTable = new JTable(dtm);

        vypisTable.getColumnModel().getColumn(6).setCellRenderer(new ClientsTableButtonRenderer());
        vypisTable.getColumnModel().getColumn(6).setCellEditor(new ClientsTableRenderer(new JCheckBox()));

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);

    }

    static class ClientsTableButtonRenderer extends JButton implements TableCellRenderer
    {
        public ClientsTableButtonRenderer()
        {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            setForeground(Color.black);
            setBackground(UIManager.getColor("Button.background"));
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }
    public class ClientsTableRenderer extends DefaultCellEditor
    {
        private final JButton button;
        private String label;
        private boolean clicked;
        private int row;

        public ClientsTableRenderer(JCheckBox checkBox)
        {
            super(checkBox);
            button = new JButton();
            button.setOpaque(true);
            button.addActionListener(e -> fireEditingStopped());
        }
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
        {
            this.row = row;

            button.setForeground(Color.black);
            button.setBackground(UIManager.getColor("Button.background"));
            label = (value == null) ? "" : value.toString();
            button.setText(label);
            clicked = true;
            return button;
        }
        public Object getCellEditorValue()
        {
            if (clicked)
            {
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new java.io.File("./rentgen"));
                chooser.setDialogTitle("Vyberte složku");
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);

                if(chooser.showOpenDialog(this.getComponent()) == JFileChooser.APPROVE_OPTION) {
                    JOptionPane.showMessageDialog(frame, loadRentgen(chooser.getSelectedFile(), druh, jmeno, vysetreniID[row]));
                }
            }
            clicked = false;
            return label;
        }

        public boolean stopCellEditing()
        {
            clicked = false;
            return super.stopCellEditing();
        }

        protected void fireEditingStopped()
        {
            super.fireEditingStopped();
        }
    }
}
