package table.tabs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class VybehyHistoriePanel extends JPanel {
    public DefaultTableModel dtm;

    public void setDtm(String[][] data) {
        dtm.setDataVector(data, new String[]{"Datum", "Oprava/úprava", "Údržbář"});
    }

    public VybehyHistoriePanel() {
        super(new BorderLayout(), false);

        String[] columns = {"Datum", "Oprava/úprava", "Údržbář"};
        String[][] data = {{}};

        dtm = new DefaultTableModel(data, columns);
        dtm.setColumnIdentifiers(columns);
        JTable vypisTable = new JTable(dtm);

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);
    }
}
