package table.tabs;

import gui.MainFrame;
import util.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;

import static model.Zamestnanec.getRozvrhKrmeni;
import static model.Zvire.getPolozkyKrmeniFromNazev;

public class RozvrhKrmeniPanel extends JPanel {
    public JTable rozpisTable;
    public DefaultTableModel dtm;
    private final JFrame frame;
    private String[][] data;
    private final String[][][][] rozvrhKrmeni;

    public RozvrhKrmeniPanel(MainFrame mainFrame, User user) {
        super(new BorderLayout(), false);
        frame = mainFrame;
        data = new String[0][8];
        rozvrhKrmeni = getRozvrhKrmeni(user);

        Date today = Calendar.getInstance().getTime();
        int dayInWeek = getDayNumberOld(today);
        String[] columns = {"Čas", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle"};

        switch(dayInWeek) {
            case 1:
                columns[7] = "Neděle (dnes)";
                break;
            case 2:
                columns[1] = "Pondělí (dnes)";
                break;
            case 3:
                columns[2] = "Úterý (dnes)";
                break;
            case 4:
                columns[3] = "Středa (dnes)";
                break;
            case 5:
                columns[4] = "Čtvrtek (dnes)";
                break;
            case 6:
                columns[5] = "Pátek (dnes)";
                break;
            case 7:
                columns[6] = "Sobota (dnes)";
                break;
        }


        for(int time = 5; time <= 22; time++) {
            String[][] temp = data.clone();
            data = new String[data.length + 1][8];
            System.arraycopy(temp, 0, data, 0, temp.length);

            data[data.length-1][0] = time + ":00";

            for(int i = 0; i < 7; i++) {
                for (int j = 0; j < 10; j++) {
                    if (rozvrhKrmeni[time - 5][i][j][0] == null)
                        break;
                    if (data[data.length - 1][i + 1] == null)
                        data[data.length - 1][i + 1] = rozvrhKrmeni[time - 5][i][j][0];
                    else
                        data[data.length - 1][i + 1] = data[data.length - 1][i + 1] + ", " + rozvrhKrmeni[time - 5][i][j][0];
                }
            }
        }

        dtm = new DefaultTableModel(data, columns);

        rozpisTable = new JTable(dtm);
        rozpisTable.setFont(new Font("MS Sans Serif", Font.BOLD, 30));

        for(int i = 0; i < rozpisTable.getRowCount(); i++) {
            rozpisTable.setRowHeight(i, 30);
        }
        for(int i = 1; i < 8; i++) {
            rozpisTable.getColumnModel().getColumn(i).setCellRenderer(new ClientsTableButtonRenderer());
            rozpisTable.getColumnModel().getColumn(i).setCellEditor(new ClientsTableRenderer(new JCheckBox()));
        }

        JScrollPane scrollPane = new JScrollPane(rozpisTable);

        add(scrollPane);
    }

    public static int getDayNumberOld(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    static class ClientsTableButtonRenderer extends JButton implements TableCellRenderer
    {
        public ClientsTableButtonRenderer()
        {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            setForeground(Color.black);
            setBackground(UIManager.getColor("Button.background"));
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }
    public class ClientsTableRenderer extends DefaultCellEditor
    {
        private final JButton button;
        private String label;
        private boolean clicked;
        private int row, column;

        public ClientsTableRenderer(JCheckBox checkBox)
        {
            super(checkBox);
            button = new JButton();
            button.setOpaque(true);
            button.addActionListener(e -> fireEditingStopped());
        }

        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
        {
            this.row = row;
            this.column = column;

            button.setForeground(Color.black);
            button.setBackground(UIManager.getColor("Button.background"));
            label = (value == null) ? "" : value.toString();
            button.setText(label);
            clicked = true;
            return button;
        }

        public Object getCellEditorValue()
        {
            if(clicked && !button.getText().equals("") && !button.getText().equals("Chyba připojení")) {
                String popUpName = "Krmení: ";
                switch(column) {
                    case 1:
                        popUpName += "Pondělí ";
                        break;
                    case 2:
                        popUpName += "Úterý ";
                        break;
                    case 3:
                        popUpName += "Středa ";
                        break;
                    case 4:
                        popUpName += "Čtvrtek ";
                        break;
                    case 5:
                        popUpName += "Pátek ";
                        break;
                    case 6:
                        popUpName += "Sobota ";
                        break;
                    case 7:
                        popUpName += "Neděle ";
                        break;
                }

                while(true) {
                    String[] options = data[row][column].split(", ");
                    int chosenKrmeni = JOptionPane.showOptionDialog(frame, rozvrhKrmeni[row][column - 1], popUpName + (row + 5) + ":00", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);

                    if(chosenKrmeni != -1) {
                        JOptionPane.showMessageDialog(frame, getPolozkyKrmeniFromNazev(options[chosenKrmeni]), "Složení krmení: " + options[chosenKrmeni], JOptionPane.PLAIN_MESSAGE);
                    } else
                        break;
                }
            }
            clicked = false;
            return label;
        }

        public boolean stopCellEditing()
        {
            clicked = false;
            return super.stopCellEditing();
        }

        protected void fireEditingStopped()
        {
            super.fireEditingStopped();
        }
    }
}
