package table.tabs;

import javax.swing.*;
import java.awt.*;

import static model.Zvire.getPocetSamcuSamic;

public class PocetSamcuSamicPanel extends JPanel {
    public PocetSamcuSamicPanel() {
        super(new BorderLayout(), false);
        String[] columnsKontrola = {"Druh", "Počet Samic", "Počet Samcu"};
        String[][] data = getPocetSamcuSamic();

        JTable vypisTable = new JTable(data, columnsKontrola);

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);
    }
}