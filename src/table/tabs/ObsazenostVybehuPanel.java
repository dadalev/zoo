package table.tabs;

import javax.swing.*;
import java.awt.*;

import static model.Zvire.getObsazenostVybehu;

public class ObsazenostVybehuPanel extends JPanel {
    public ObsazenostVybehuPanel() {
        super(new BorderLayout(), false);

        String[] columns = {"Oblast", "Výběh", "Volná místa"};
        String[][] data = getObsazenostVybehu();

        JTable vypisTable = new JTable(data, columns);

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);
    }
}
