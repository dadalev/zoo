package table.tabs;

import javax.swing.*;
import java.awt.*;

import static model.Zvire.getPocetMladat;

public class PocetMladatPanel extends JPanel {
    public PocetMladatPanel() {
        super(new BorderLayout(), false);
        String[] columnsKontrola = {"Druh", "Počet"};
        String[][] data = getPocetMladat();

        JTable vypisTable = new JTable(data, columnsKontrola);

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);
    }
}
