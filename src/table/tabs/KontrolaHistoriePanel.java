package table.tabs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;


public class KontrolaHistoriePanel extends JPanel {
    public DefaultTableModel dtm;

    public void setDtm(String[][] data) {
        dtm.setDataVector(data, new String[]{"Datum", "Druh", "Jméno", "Váha", "Potíže"});
    }

    public KontrolaHistoriePanel() {
        super(new BorderLayout(), false);

        String[] columns = {"Datum", "Druh", "Jméno", "Váha", "Potíže"};
        String[][] data = {{}};

        dtm = new DefaultTableModel(data, columns);
        dtm.setColumnIdentifiers(columns);
        JTable vypisTable = new JTable(dtm);

        JScrollPane scrollPane = new JScrollPane(vypisTable);

        add(scrollPane);
    }
}
