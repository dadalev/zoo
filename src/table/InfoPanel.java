package table;

import gui.MainFrame;
import util.User;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

import static model.Vybehy.*;
import static model.Zvire.*;

public class InfoPanel extends JPanel{
    private final JComboBox<String> druhComboBox;
    private final JComboBox<String> oblastCombo;

    public InfoPanel(MainFrame mainFrame, User user) {//tabulky z informacemi
        super(new GridBagLayout(), false);
        GridBagConstraints c = new GridBagConstraints();

        TabsPanel tabsPanel = new TabsPanel(mainFrame, user);

        JLabel druhLabel = new JLabel("Druh: ");
        druhComboBox = new JComboBox<>(getDruhy());

        JLabel jmenoLabel = new JLabel("Jméno :");
        JComboBox<String> jmenoComboBox = new JComboBox<>(getJmena(-1));

        JLabel oblastLabel = new JLabel("Oblast:");
        oblastCombo = new JComboBox<>(getOblasti());

        JLabel vybehLabel = new JLabel("Výběh:");
        JComboBox<String> vybehCombo = new JComboBox<>(getVybehy(-1));

        c.fill = GridBagConstraints.BOTH;

        c.gridx = 0;
        c.gridy = 0;
        if(user.getPage() == 0) {
            add(oblastLabel, c);
        } else {
            add(druhLabel, c);
        }
        c.gridx = 1;
        c.gridy = 0;
        if(user.getPage() == 0) {
            add(oblastCombo, c);
            oblastCombo.setSize(oblastCombo.getPreferredSize());
        } else {
            add(druhComboBox, c);
            druhComboBox.setSize(druhComboBox.getPreferredSize());
        }

        c.gridx = 0;
        c.gridy = 1;
        if(user.getPage() == 0) {
            add(vybehLabel, c);
        } else {
            add(jmenoLabel, c);
        }
        c.gridx = 1;
        c.gridy = 1;
        if(user.getPage() == 0) {
            add(vybehCombo, c);
            vybehCombo.setSize(vybehCombo.getPreferredSize());
        } else {
            add(jmenoComboBox, c);
            jmenoComboBox.setSize(jmenoComboBox.getPreferredSize());
        }

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        add(tabsPanel, c);

        druhComboBox.addActionListener(e -> {
            String[] jmena1 = getJmena(getIdFromDruh(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString()));
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(jmena1);
            jmenoComboBox.setModel(model);
        });

        jmenoComboBox.addActionListener(e -> {
            String[][] dataVysetreni = getTableDataVysetreni(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(), Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString());
            tabsPanel.setDataVysetreni(dataVysetreni, Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(), Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString());

            String[][] dataKontrola = getTableDataKontrola(Objects.requireNonNull(druhComboBox.getSelectedItem()).toString(), Objects.requireNonNull(jmenoComboBox.getSelectedItem()).toString());
            tabsPanel.setDtmKontrola(dataKontrola);
        });

        oblastCombo.addActionListener(e -> {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(getVybehy(getIdFromOblast(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString())));
            vybehCombo.setModel(model);
        });

        vybehCombo.addActionListener(e -> {
            String[][] dataVybehy = getTableDataVybehy(Objects.requireNonNull(oblastCombo.getSelectedItem()).toString(), Objects.requireNonNull(vybehCombo.getSelectedItem()).toString());
            tabsPanel.setDtmVybehy(dataVybehy);
        });
    }
}
