package util;

/**id  name
 * 0   Údržbář
 * 1   Ošetřovatel
 * 2   Veterinář
 * 3   Kurátor
 * 4   Admin
 */
public class User {
    int id;
    int page;
    String position;
    String login;

    @Override
    public String toString() {
        return "User{" + "id = " + id + ", page = " + page + ", position = " + position + ", login = " + login + '}';
    }

    public User(int id, String position, String login) {
        this.id = id;
        this.page = id;
        this.position = position;
        this.login = login;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public int getId() {
        return id;
    }

    public String getPosition() {
        return position;
    }

    public String getLogin() {
        return login;
    }
}
