package util;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class StringNormalization {
    public static String normalizeString(final String string) {
        if (string == null) {
            return null;
        }

        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

        String temp = string.trim();
        temp = Normalizer.normalize(temp, Normalizer.Form.NFD);
        temp = pattern.matcher(temp).replaceAll("");
        temp = temp.replaceAll("[\uFEFF-\uFFFF]", ""); // remove 'ZERO WIDTH NO-BREAK SPACE' chars (U+FEFF)

        return temp.toLowerCase();
    }
}
