package model;

import util.User;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static model.Zvire.isDate;

public class Vybehy {
    public static String ulozZazemi(String oblast, String nazev) {//vytvoření nového zázemí
        if(oblast.equals("Vyberte Oblast")) {
            return "Vyberte Oblast";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("Select OBLASTIZOOID from OBLASTIZOO where NAZEV = '" + oblast + "'").executeQuery();
            rs1.next();
            con.prepareStatement("insert into ZAZEMIZAMESTNACU(NAZEVPROSTORU,OBLASTIZOOID) values ('" + nazev + "', " + rs1.getInt(1) + ")").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            e.printStackTrace();
            return "Connection Error";
        }
    }

    public static String ulozOblast(String nazev, String poloha) {//vytvoření nové oblasti
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("INSERT into OBLASTIZOO(NAZEV,POLOHA) values ('" + nazev + "','" + poloha + "')").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            e.printStackTrace();
            return "Chyba připojení";
        }
    }

    public static String ulozVybeh(String oblast, String nazev, String typ, String rozloha, String maxPocet) {//vytvoření nového výběhu
        if(oblast.equals("Vyberte Oblast")) {
            return "Vyberte Oblast";
        }
        int maxPocetInt;
        try {
            maxPocetInt = Integer.parseInt(maxPocet);
        } catch (Exception e) {
            return "Maxinálí počet kusů musí být číslo";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("Select OBLASTIZOOID from OBLASTIZOO where NAZEV = '" + oblast + "'").executeQuery();
            rs1.next();
            con.prepareStatement("insert into VYBEHY(OBLASTIZOOID,NAZEV,TYP,ROZLOHA,MAXPOCETKS) values (" + rs1.getInt(1) + ",'" + nazev + "','" + typ + "','" + rozloha+ "', " + maxPocetInt + ")").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            e.printStackTrace();
            return "Chyba připojení";
        }
    }

    public static String ulozOpravaUpravaVybehu(String oblast, String vybeh, String opravaUprava, String datum, User user) {//vytvoření nového úúravy/opravy u výběhu
        if(vybeh.equals("Vyberte Výběh"))
            return "Nevybraný výběh";
        if(oblast.equals("Vyberte Oblast")) {
            String[] asd = vybeh.split("\\)", 0);
            vybeh = asd[1].substring(1);
        }
        if(datum.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            datum = dtf.format(LocalDateTime.now());
        } else if(!isDate(datum)){
            return "Špatné datum";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("select z.ZAMESTNANCIid from PRIHLASOVACIUDAJE p, ZAMESTNANCI z where  p.LOGIN = '" + user.getLogin() + "' and p.ZAMESTNANCIID = z.ZAMESTNANCIID").executeQuery();
            rs1.next();
            ResultSet rs2 = con.prepareStatement("select VYBEHYID from VYBEHY where NAZEV = '" + vybeh + "'").executeQuery();
            rs2.next();
            con.prepareStatement("INSERT into OPRAVAVYBEHU(DATUM,COBYLOPROVEDENO,ZAMESTNANCIID,VYBEHYID)values (TO_DATE('" + datum + "', 'dd/MM/yy'),'" + opravaUprava + "'," + rs1.getInt(1) + "," + rs2.getInt(1) + ")").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String[][] getTableDataVybehy(String oblast, String vybeh) {//info o výběhu
        if(vybeh.equals("Vyberte Vybeh"))
            return new String[][] {{"Nevybraný výběh","ERROR","ERROR"}};
        if(oblast.equals("Vyberte Oblast")) {
            String[] temp = vybeh.split("\\)", 0);
            vybeh = temp[1].substring(1);
        }
        String[][] out = new String[0][3];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("Select o.DATUM,o.COBYLOPROVEDENO,z.JMENO,z.PRIJMENI, p.LOGIN " +
                    "from OPRAVAVYBEHU o, ZAMESTNANCI z ,PRIHLASOVACIUDAJE p, VYBEHY v " +
                    "where o.ZAMESTNANCIID = z.ZAMESTNANCIID and p.ZAMESTNANCIID = z.ZAMESTNANCIID " +
                    "and v.VYBEHYID = o.VYBEHYID " +
                    "and v.NAZEV = '" + vybeh + "'").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][3];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(1).substring(8, 10) + "." + rs.getString(1).substring(5, 7) + "." + rs.getString(1).substring(0, 4);
                out[out.length-1][1] = rs.getString(2);
                out[out.length-1][2] = rs.getString(3) + " " + rs.getString(4) + " (" + rs.getString(5) + ")";

            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR"}};
        }
    }

    public static String[] getOblasti() {//výpis oblastí
        String[] out = {"Vyberte Oblast"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT NAZEV from OBLASTIZOO").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[] getVybehy(int id) {//výpis všech výběhů nebo v dané oblasti
        String[] out = {"Vyberte Výběh"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs;
            if(id == -1) {
                rs = con.prepareStatement("SELECT o.nazev, v.Nazev FROM OBLASTIZOO o ,Vybehy v where o.OBLASTIZOOID = v.OBLASTIZOOID").executeQuery();
                while(rs.next()) {
                    String[] temp = out.clone();
                    out = new String[out.length + 1];
                    System.arraycopy(temp, 0, out, 0, temp.length);
                    out[out.length-1] = "(" + rs.getString(1) + ") " + rs.getString(2);
                }
            } else {
                rs = con.prepareStatement("SELECT Nazev FROM Vybehy where OBLASTIZOOID = " + id).executeQuery();
                while(rs.next()) {
                    String[] temp = out.clone();
                    out = new String[out.length + 1];
                    System.arraycopy(temp, 0, out, 0, temp.length);
                    out[out.length-1] = rs.getString(1);
                }
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }
}
