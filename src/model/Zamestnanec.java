package model;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.util.encoders.Hex;
import util.User;

import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static util.StringNormalization.*;

public class Zamestnanec {
    public static String[] getZazemi() {//výpis zázemí
        String[] out = {"Vyberte Zázemí"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select NAZEVPROSTORU from ZAZEMIZAMESTNACU").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[][][][] getRozvrhKrmeni(User user) {//info o krmení pro ošetřovatele na celý týden
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
        LocalDate today = LocalDate.now();
        String[][][][] krmeniTable = new String[18][7][10][2];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("SELECT ZAMESTNANCIID from PRIHLASOVACIUDAJE WHERE LOGIN = '" + user.getLogin() + "'").executeQuery();
            rs1.next();
            for(int i = 0; i <= 6; i++) {
                int[] krmeniIndex = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                String tempDay = dtf.format(today.minusDays(today.getDayOfWeek().getValue() - i - 1));
                ResultSet rs2 = con.prepareStatement("SELECT r.NAZEV, TO_CHAR(PRVNIKRMENI, 'HH24:MI') FROM ZVIRATA z, PRIRAZENEZVIRE p ,KONKRETNIKRMENI k ,ROZPISKRMENI r " +
                        "where p.ZVIRATAID = z.ZVIRATAID " +
                        "and p.ZAMESTNANCIID = " + rs1.getInt(1) + " " +
                        "and k.ZVIRATAID = z.ZVIRATAID " +
                        "and k.ROZPISKRMENIID = r.ROZPISKRMENIID " +
                        "and datum_krmeni(r.ROZPISKRMENIID,TO_DATE('" + tempDay + "', 'dd/MM/yy')) = TO_DATE('" + tempDay + "', 'dd/MM/yy') " +
                        "GROUP by r.NAZEV, TO_CHAR(PRVNIKRMENI, 'HH24:MI') " +
                        "order by TO_CHAR(PRVNIKRMENI, 'HH24:MI')").executeQuery();
                while(rs2.next()) {
                    ResultSet rs3 = con.prepareStatement("SELECT z.JMENO ,t.NAZEVCESKY from ZVIRATA z, KONKRETNIKRMENI k ,ROZPISKRMENI r ,TAXONOMICKEOZNACENI t " +
                            "where  k.ZVIRATAID = z.ZVIRATAID " +
                            "and t.TAXONOMICKEOZNACENIID = z.TAXONOMICKEOZNACENIID " +
                            "and k.ROZPISKRMENIID = r.ROZPISKRMENIID " +
                            "and r.NAZEV = '" + rs2.getString(1) + "'").executeQuery();
                    krmeniTable[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5][i][krmeniIndex[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5]][0] = rs2.getString(1);
                    while(rs3.next()) {
                        if(krmeniTable[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5][i][krmeniIndex[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5]][1] == null)
                            krmeniTable[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5][i][krmeniIndex[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5]][1] = "       " + rs3.getString(2) + " - " + rs3.getString(1);
                        else
                            krmeniTable[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5][i][krmeniIndex[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5]][1] += "\n       " + rs3.getString(2) + " - " + rs3.getString(1);
                    }
                    krmeniIndex[Integer.parseInt(rs2.getString(2).substring(0,2)) - 5]++;
                }
            }
            con.close();
            return krmeniTable;
        } catch(Exception e) {
            krmeniTable[0][0][0][0] = "Chyba připojení";
            return krmeniTable;
        }
    }

    public static String[] getOsetrovatel() {//výpis ošetřovatelů
        String[] out = {"Vyberte Ošetřovatele"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT Z.JMENO, z.PRIJMENI, p.login  FROM ZAMESTNANCI z , PRIHLASOVACIUDAJE  p where z.ZAMESTNANCIID = p.ZAMESTNANCIID and p.role = 1").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1) + " " + rs.getString(2) + " (" + rs.getString(3) + ")";
            }
            con.close();
            return out;
        } catch(Exception e) {
            e.printStackTrace();
            return new String[] {"Chyba připojení"};
        }
    }

    public static String resetPw(String login, int pwType){//resetuje heslo daného uživatele
        char[] pw = getPw(login, pwType);
        String heslo64 = hash(pw);
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("update prihlasovaciudaje set heslo = '" + heslo64 + "' where login = '" + login + "'").executeQuery();

            ResultSet rs = con.prepareStatement("select heslo from prihlasovaciudaje where login = '" + login + "'").executeQuery();
            if(!rs.next())
                return "Špatný login";
            con.close();
            return Arrays.toString(pw);
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String changePw(User user, char[] oldPw, char[] newPw, char[] newAgainPw){//změna hesla uživatele
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select heslo from prihlasovaciudaje where login = '" + user.getLogin() + "'").executeQuery();
            rs.next();
            String hesloOld64 = hash(oldPw);
            if(!rs.getString(1).equals(hesloOld64)) {
                con.close();
                return "Špatné heslo";
            }
            if(newPw.length < 8) {
                return "Nové heslo musí být alespoň 8 znaků dlouhé";
            }
            String heslo64 = hash(newPw);
            String hesloAgain64 = hash(newAgainPw);
            if(!heslo64.equals(hesloAgain64)) {
                con.close();
                return "Hesla se neshodují";
            }
            con.prepareStatement("update prihlasovaciudaje set heslo = '" + heslo64 + "' where login = '" + user.getLogin() + "'").executeQuery();
            con.close();
            return "Heslo změněno";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String[] newZam(String jmeno, String prijmeni, int rc, int pwType, int idRole, String zazemi) {//přidání nového zaměstnance
        if(zazemi.equals("Vyberte zázemí")) {
            return new String[]{"Nevybrané zázemí", "Nevybrané zázemí"};
        }
        String login = getLogin(jmeno.toLowerCase(), prijmeni.toLowerCase());
        int nextLoginNum = getNextLoginNum(login, 1);
        if(nextLoginNum == 0)
            return new String[]{"Chyba připojení", "Chyba připojení"};
        login = login + nextLoginNum;//(prijmeni 5) + (jmeno 2) + cislo
        char[] pw = getPw(login, pwType);
        String heslo64 = hash(pw);
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("begin NOVY_ZAMESTNANEC('" + jmeno + "','" + prijmeni + "','" + rc + "','" + login + "','" + heslo64 + "'," + idRole + ", '" + zazemi + "'); end;").executeQuery();
            con.close();
            return new String[]{login, Arrays.toString(pw)};
        } catch(Exception e) {
            return new String[]{"Chyba připojení", "Chyba připojení"};
        }
    }

    static String hash(char[] pw) {// hash funkce
        byte[] bytesIn = String.valueOf(pw).getBytes(StandardCharsets.UTF_8); //string to bytes

        SHA256Digest d = new SHA256Digest();//set hash sha256
        d.update(bytesIn, 0, bytesIn.length);           //do
        byte[] bytesOut = new byte[d.getDigestSize()];  //some
        d.doFinal(bytesOut, 0);                         //magic

        return new String(Hex.encode(bytesOut));//bytes to hex
    }

    private static char[] getPw(String login, int pwType){//generace defauftního hesla
        String prijmeni1 = login.substring(0, 1).toUpperCase();
        String jmeno1 = login.substring(login.length()-3, login.length()-2).toUpperCase();

        if(pwType == 1) {
            return (String.format("%06d", (int) (Math.random() * 999999)) + jmeno1 + prijmeni1).toCharArray();
        } else if(pwType == 2) {
            return String.format("%08d", (int) (Math.random() * 99999999)).toCharArray();
        } else {
            return ("123456" + jmeno1 + prijmeni1).toCharArray();
        }
    }

    private static String getLogin(String jmeno, String prijmeni){//generuje login bez cisla
        String prijmeni5;
        String jmeno2;
        if(prijmeni.length() > 5) {
            prijmeni5 = prijmeni.substring(0, 5);
        } else {
            prijmeni5 = prijmeni;
        }
        if(jmeno.length() > 2) {
            jmeno2 = jmeno.substring(0, 2);
        } else {
            jmeno2 = jmeno;
        }
        return normalizeString(prijmeni5 + jmeno2);
    }

    private static int getNextLoginNum(String login, int num){//generuje cislo k loginu
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select login from prihlasovaciudaje").executeQuery();
            while(rs.next()) {
                if(rs.getString(1).equals(login + num)){
                    num = getNextLoginNum(login, num+1);
                    break;
                }
            }
            return num;
        } catch(Exception e) {
            return 0;
        }
    }
}
