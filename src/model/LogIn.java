package model;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.util.encoders.Hex;
import util.User;

import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.DriverManager;
import java.sql.ResultSet;

import static model.Zamestnanec.hash;

public class LogIn {
    public static User loginAndGetUser(String login, char[] pw) {//kontrola hesla a vrátí info o uživateli
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("select heslo from prihlasovaciudaje where login = '" + login + "'").executeQuery();
            rs1.next();

            String heslo64 = hash(pw);

            if(rs1.getString(1).equals(heslo64)) {
                ResultSet rs2 = con.prepareStatement("SELECT zjisteni_role ('" + login + "') FROM dual").executeQuery();
                rs2.next();

                Array arr = rs2.getArray(1);
                String[] s = (String[])arr.getArray();

                con.close();
                return new User(Integer.parseInt(s[0]), s[1], login);
            }
            con.close();
            return new User(-1, "Špatný login nebo heslo", "unknown");
        } catch(Exception e) {
            return new User(-1, "Chyba připojení", "unknown");
        }
    }
}
