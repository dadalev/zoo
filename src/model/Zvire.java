package model;

import util.User;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class Zvire {
    public static String priraditOsetrovatele(String druh, String jmeno, String osetrovatel) {//přiřadit ošetřovatele ke zvířeti
        if(jmeno.equals("Vyberte Jméno"))
            return "Nevybrané jméno";
        if(druh.equals("Vyberte Druh")) {
            String[] temp = jmeno.split("\\)", 0);
            druh = temp[0].substring(1);
            jmeno = temp[1].substring(1);
        }
        if(osetrovatel.equals("Vyberte Ošetřovatele"))
            return "Nevybraný ošetřovatel";
        osetrovatel = osetrovatel.split("\\(")[1].split("\\)")[0];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("SELECT z.ZVIRATAID from ZVIRATA z, TAXONOMICKEOZNACENI t where z.TAXONOMICKEOZNACENIID = t.TAXONOMICKEOZNACENIID and t.NAZEVCESKY = '" + druh + "' and z.JMENO = '" + jmeno + "'").executeQuery();
            rs1.next();
            ResultSet rs2 = con.prepareStatement("SELECT z.ZAMESTNANCIID  FROM ZAMESTNANCI z , PRIHLASOVACIUDAJE  p where z.ZAMESTNANCIID = p.ZAMESTNANCIID and p.LOGIN = '" + osetrovatel + "'").executeQuery();
            rs2.next();
            con.prepareStatement("insert into PRIRAZENEZVIRE(ZVIRATAID,ZAMESTNANCIID) values (" + rs1.getInt(1) + ", " + rs2.getInt(1) + ")").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String priraditKrmeni(String druh, String jmeno, String krmeni) {//přiřadit krmení ke zvířeti
        if(jmeno.equals("Vyberte Jméno"))
            return "Nevybrané jméno";
        if(krmeni.equals("Vyberte Krmení"))
            return "Nevybrané krmení";
        if(druh.equals("Vyberte Druh")) {
            String[] temp = jmeno.split("\\)", 0);
            druh = temp[0].substring(1);
            jmeno = temp[1].substring(1);
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("SELECT z.ZVIRATAID from ZVIRATA z, TAXONOMICKEOZNACENI t where z.TAXONOMICKEOZNACENIID = t.TAXONOMICKEOZNACENIID and t.NAZEVCESKY = '" + druh + "' and z.JMENO = '" + jmeno + "'").executeQuery();
            rs1.next();
            ResultSet rs2 = con.prepareStatement("Select ROZPISKRMENIID from ROZPISKRMENI where NAZEV = '" + krmeni + "'").executeQuery();
            rs2.next();
            con.prepareStatement("insert into KONKRETNIKRMENI(ZVIRATAID,ROZPISKRMENIID) values (" + rs1.getInt(1) + "," + rs2.getInt(1) + ")").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String[] getNazvyKrmeni(User user) {//výpis vřech krmení
        String[] out = {"Vyberte Krmeni"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("SELECT ZAMESTNANCIID from PRIHLASOVACIUDAJE where LOGIN = '" + user.getLogin() + "'").executeQuery();
            rs1.next();
            ResultSet rs = con.prepareStatement("SELECT NAZEV FROM ROZPISKRMENI WHERE ZAMESTNANCIID = " + rs1.getInt(1)).executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String ulozKrmeni(String nazev, String prvniKrmeniDatum, String jakCasto, String vKolik, String[][] obsahKrmeni, User user) {//uloží nové krmení
        int jakCastoInt;
        try{
            jakCastoInt = Integer.parseInt(jakCasto);
            if(jakCastoInt <= 0)
                return "Jak často musí kladné číslo";
        } catch (Exception e) {
            return "Jak často musí být celé číslo";
        }
        if(!isTimeNoMinutes(vKolik))
            return "Čas není zadán správně (HH:MM)";
        try{
            Integer.parseInt(vKolik.substring(0, 2));
        } catch (Exception e) {
            vKolik = "0" + vKolik;
        }
        if(Integer.parseInt(vKolik.substring(0, 2)) < 5 || Integer.parseInt(vKolik.substring(0, 2)) > 22)
            return "Čas je mimo pracovní dobu";
        for(String[] strings : obsahKrmeni) {
            if(strings[0].equals("Vyberte Položku")) {
                return "Nevybraná položka";
            }
            if(strings[1].equals("")) {
                return "Zadejte Počet";
            }
        }
        if(prvniKrmeniDatum.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            prvniKrmeniDatum = dtf.format(LocalDateTime.now());
        } else if(!isDate(prvniKrmeniDatum)){
            return "Špatné datum";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("SELECT ZAMESTNANCIID from PRIHLASOVACIUDAJE where LOGIN = '" + user.getLogin() + "'").executeQuery();
            rs1.next();
            con.prepareStatement("INSERT into ROZPISKRMENI(NAZEV,PRVNIKRMENI,JAKCASTO,ZAMESTNANCIID) values('" + nazev + "', TO_DATE('" + prvniKrmeniDatum + " " + vKolik + "', 'dd/MM/yy HH24:MI'), " + jakCastoInt + ", " + rs1.getInt(1) + ")").executeQuery();
            ResultSet rs2 = con.prepareStatement("SELECT SEQ_ROZPISKRMENI.currval FROM dual").executeQuery();
            rs2.next();
            for (String[] strings : obsahKrmeni) {
                ResultSet rs3 = con.prepareStatement("Select KRMENIID from KRMENI where NAZEV = '" + strings[0] + "'").executeQuery();
                rs3.next();
                con.prepareStatement("insert into Polozka(POCET,KRMENIID,ROZPISKRMENIID) values('" + strings[1] + "', " + rs3.getInt(1) + ", " + rs2.getInt(1) + ")").executeQuery();
            }
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            e.printStackTrace();
            return "Chyba připojení";
        }
    }

    public static String[] getPolozkyKrmeni() {//výpis položek krmení
        String[] out = {"Vyberte Položku"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT Nazev FROM krmeni").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[] getPolozkyKrmeniFromNazev(String nazev) {//výpis položek určitého krmení
        String[] out = {};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs1 = con.prepareStatement("select ROZPISKRMENIID from ROZPISKRMENI where NAZEV = '" + nazev + "'").executeQuery();
            rs1.next();
            ResultSet rs2 = con.prepareStatement("select KRMENIID, POCET from POLOZKA where ROZPISKRMENIID = " + rs1.getString(1)).executeQuery();
            while(rs2.next()) {
                ResultSet rs3 = con.prepareStatement("select NAZEV from KRMENI where KRMENIID = " + rs2.getString(1)).executeQuery();
                rs3.next();

                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs3.getString(1) + " - " + rs2.getString(2);
            }
            con.close();
            return out;
        } catch(Exception e) {
            e.printStackTrace();
            return new String[] {"Chyba připojení"};
        }
    }

    public static String newZvire(String druh, String jmeno, String pohlavi, String vaha, String dPorizeni, String dNarozeni,
            String puvod, String cisloPlemene, String trida, String celed, String rad, String rod, String oblast, String vybeh, User role) {//přidání nového zvířete
        if(pohlavi.equals("Vyberte Pohlaví")) {
            return "ERROR";
        }

        switch (pohlavi){
            case "Samec":
                pohlavi = "M";
                break;
            case "Samice":
                pohlavi = "F";
                break;
        }

        double vahaDouble;
        try{
            vahaDouble = Double.parseDouble(vaha);
            if(vahaDouble <= 0)
                return "Váha musí být kladné číslo";
        } catch (Exception e) {
            return "Váha musí být číslo (počet kg)";
        }

        if(dPorizeni.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            dPorizeni = dtf.format(LocalDateTime.now());
        } else if(!isDate(dPorizeni)){
            return "Špatné datum pořízení";
        }

        if(dNarozeni.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            dNarozeni = dtf.format(LocalDateTime.now());
        } else if(!isDate(dNarozeni)){
            return "Špatné datum narození";
        }

        int cisloPlemeneInt;
        try{
            cisloPlemeneInt = Integer.parseInt(cisloPlemene);
            if(cisloPlemeneInt <= 0)
                return "Číslo v plemené knize musí být kladné číslo";
        } catch (Exception e) {
            return "Číslo v plemené knize musí celé číslo";
        }

        if(trida.equals("Vyberte Třídu")) {
            return "Nevybraná Třída";
        }

        if(vybeh.equals("Vyberte Výběh")) {
            return "Nevybraný Výběh";
        }

        if(oblast.equals("Vyberte Oblast")) {
            String[] asd = vybeh.split("\\)", 0);
            vybeh = asd[1].substring(1);
        }

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("begin NOVE_ZVIRE(" +
                    "'" + role.getLogin() + "'," +
                    "'" + jmeno + "'," +
                    "TO_DATE('" + dNarozeni + "', 'dd/MM/yy')," +
                    "'" + pohlavi +"'," +
                    "" + vahaDouble +"," +
                    "'" + cisloPlemeneInt +"'," +
                    "'" + puvod +"'," +
                    "TO_DATE('" + dPorizeni + "', 'dd/MM/yy')," +
                    "'" + trida +"'," +
                    "'" + druh +"'," +
                    "'" + celed +"'," +
                    "'" + rad +"'," +
                    "'" + rod +"'," +
                    "'" + vybeh +"');end;").executeQuery();
            con.close();
            return "Přidáno";
        } catch (Exception e) {
            return "Chyba připojení";
        }
    }

    public static String[] getTrida() {//výpis tříd
        String[] out = {"Vyberte Třídu"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT TRIDANAZEV from TRIDACODEBOOK").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[] getDruhy() {//výpis druhů
        String[] out = {"Vyberte Druh"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT NAZEVCESKY from TAXONOMICKEOZNACENI").executeQuery();
            while(rs.next()) {
                String[] temp = out.clone();
                out = new String[out.length + 1];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1] = rs.getString(1);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[] getJmena(int id) {//výpis všech jmen nebo jmen z určitého druhu
        String[] out = {"Vyberte Jméno"};
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs;
            if(id == -1) {
                rs = con.prepareStatement("SELECT t.NAZEVCESKY, z.jmeno from TAXONOMICKEOZNACENI t, ZVIRATA z where t.TAXONOMICKEOZNACENIID = z.TAXONOMICKEOZNACENIID").executeQuery();
                while(rs.next()) {
                    String[] temp = out.clone();
                    out = new String[out.length + 1];
                    System.arraycopy(temp, 0, out, 0, temp.length);
                    out[out.length-1] = "(" + rs.getString(1) + ") " + rs.getString(2);
                }
            } else {
                rs = con.prepareStatement("SELECT jmeno from ZVIRATA where TAXONOMICKEOZNACENIID =" + id).executeQuery();
                while(rs.next()) {
                    String[] temp = out.clone();
                    out = new String[out.length + 1];
                    System.arraycopy(temp, 0, out, 0, temp.length);
                    out[out.length-1] = rs.getString(1);
                }
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[] {"Chyba připojení"};
        }
    }

    public static String[][] getTableDataVysetreni(String druh, String jmeno) {//info o zvířeti (vyšetření)
        if(jmeno.equals("Vyberte Jméno"))
            return new String[][] {{"Nevybrané Jméno","ERROR","ERROR","ERROR","ERROR","ERROR","ERROR"}};
        if(druh.equals("Vyberte Druh")) {
            String[] temp = jmeno.split("\\)", 0);
            druh = temp[0].substring(1);
            jmeno = temp[1].substring(1);
        }
        String[][] out = new String[0][8];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT V.DATUM,Z.JMENO,T.NAZEVCESKY,V.PRIZNAKY,V.NALEZ,V.LECBA, COUNT (R.VYSETRENIZVIRETEID), V.VYSETRENIZVIRETEID FROM RENTEGENOVESNIMKY R  RIGHT OUTER JOIN  VYSETRENIZVIRETE V ON( R.VYSETRENIZVIRETEID = V.VYSETRENIZVIRETEID)JOIN ZVIRATA Z ON (V.ZVIRATAID = Z.ZVIRATAID)" +
                                                "JOIN TAXONOMICKEOZNACENI T ON (T.TAXONOMICKEOZNACENIID = Z.TAXONOMICKEOZNACENIID)" +
                                                "WHERE Z.JMENO = '"+ jmeno +"' AND T.NAZEVCESKY = '"+ druh +"'"+
                                                "GROUP BY V.VYSETRENIZVIRETEID,V.DATUM,Z.JMENO,T.NAZEVCESKY,V.PRIZNAKY,V.NALEZ,V.LECBA,V.VYSETRENIZVIRETEID " +
                                                "ORDER BY V.DATUM DESC").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][8];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(1).substring(8, 10) + "." + rs.getString(1).substring(5, 7) + "." + rs.getString(1).substring(0, 4);
                out[out.length-1][1] = rs.getString(2);
                out[out.length-1][2] = rs.getString(3);
                out[out.length-1][3] = rs.getString(4);
                out[out.length-1][4] = rs.getString(5);
                out[out.length-1][5] = rs.getString(6);
                out[out.length-1][6] = rs.getString(7);
                out[out.length-1][7] = rs.getString(8);
            }

            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR","ERROR","ERROR","ERROR","ERROR"}};
        }
    }

    public static String[][] getTableDataKontrola(String druh, String jmeno) {//info o zvířeti (kontroly)
        if(jmeno.equals("Vyberte Jméno"))
            return new String[][] {{"Nevybrané Jméno","ERROR","ERROR","ERROR","ERROR","ERROR","ERROR"}};
        if(druh.equals("Vyberte Druh")) {
            String[] asd = jmeno.split("\\)", 0);
            druh = asd[0].substring(1);
            jmeno = asd[1].substring(1);
        }
        String[][] out = new String[0][5];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT k.datum, t.NAZEVCESKY, z.JMENO,k.VAHA,k.ZJISTENEPOTIZEZVIRETE from KONTROLAZVIRETE k " +
                                                "join ZVIRATA z on (k.ZVIRATAID = z.ZVIRATAID) " +
                                                "join TAXONOMICKEOZNACENI t on ( t.TAXONOMICKEOZNACENIID = z.TAXONOMICKEOZNACENIID) " +
                                                "where t.NAZEVCESKY = '" + druh + "' and z.Jmeno = '" + jmeno + "'" +
                                                "ORDER BY  k.datum  DESC").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][5];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(1).substring(8, 10) + "." + rs.getString(1).substring(5, 7) + "." + rs.getString(1).substring(0, 4);
                out[out.length-1][1] = rs.getString(2);
                out[out.length-1][2] = rs.getString(3);
                out[out.length-1][3] = rs.getString(4);
                out[out.length-1][4] = rs.getString(5);
            }
            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR","ERROR","ERROR"}};
        }
    }

    public static int getIdFromOblast(String nazev) {//vrátí id oblasti
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT oblastizooid from oblastizoo where NAZEV = '" + nazev +"'").executeQuery();
            rs.next();
            int x = rs.getInt(1);
            con.close();
            return x;
        } catch(Exception e) {
            return -1;
        }
    }

    public static int getIdFromDruh(String druh) {//vrátí id druhu
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT TAXONOMICKEOZNACENIID from TAXONOMICKEOZNACENI where NAZEVCESKY = '" + druh +"'").executeQuery();
            rs.next();
            int x = rs.getInt(1);
            con.close();
            return x;
        } catch(Exception e) {
            return -1;
        }
    }

    public static String ulozKontrola(String datum, String jmeno, String druh, String potize, String vaha, User user) {//uloží kontrolu
        if(jmeno.equals("Vyberte Jméno"))
            return "Nevybrané jméno";
        if(druh.equals("Vyberte Druh")) {
            String[] asd = jmeno.split("\\)", 0);
            druh = asd[0].substring(1);
            jmeno = asd[1].substring(1);
        }
        double vahaDouble;
        try{
            vahaDouble = Double.parseDouble(vaha);
            if(vahaDouble <= 0)
                return "Váha musí být kladné číslo";
        } catch (Exception e) {
            return "Váha musí být číslo (počet kg)";
        }
        if(datum.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            datum = dtf.format(LocalDateTime.now());
        } else if(!isDate(datum)){
            return "Špatné datum";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("begin NOVE_KONTROLAZVIRETE(TO_DATE('" + datum + "', 'dd/MM/yy'),'" + jmeno +"','" + druh +"'," + vahaDouble +",'" + potize +"','" + user.getLogin() +"'); end;").executeQuery();
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String ulozVysetreni(String datum, String jmeno, String druh, String priznaky, String nalez, String lecba, User user, File[] files) {//uloží vyšetření
        if(jmeno.equals("Vyberte Jméno"))
            return "Nevybrané jméno";
        if(druh.equals("Vyberte Druh")) {
            String[] asd = jmeno.split("\\)", 0);
            druh = asd[0].substring(1);
            jmeno = asd[1].substring(1);
        }
        if(datum.equals("dnes")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
            datum = dtf.format(LocalDateTime.now());
        } else if(!isDate(datum)){
            return "Špatné datum";
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            con.prepareStatement("begin NOVE_VYSETRENIZVIRETE(TO_DATE('" + datum + "', 'dd/MM/yy'),'" + jmeno +"','" + druh +"','" + priznaky +"','" + nalez +"','" + lecba +"','" + user.getLogin() +"'); end;").executeQuery();
            ResultSet rs = con.prepareStatement("select SEQ_VYSETRENIZVIRETE.currval from DUAL").executeQuery();
            rs.next();
            int index = rs.getInt(1);
            ulozRentgen(files, index, con);
            con.close();
            return "Uloženo";
        } catch(Exception e) {
            e.printStackTrace();
            return "Chyba připojení";
        }
    }

    public static void ulozRentgen(File[] files, int index, java.sql.Connection con) throws SQLException, IOException {//uloží rentgen z počítače do databáze
        if(files != null) {
            for (File file : files) {
                con.prepareStatement("alter index BRV1.PK_RENTEGENOVESNIMKY rebuild").executeQuery();
                PreparedStatement ps = con.prepareStatement("insert into RENTEGENOVESNIMKY(VYSETRENIZVIRETEID, SNIMEK) values(?,?)");
                ps.setInt(1, index);
                FileInputStream fin = new FileInputStream(file);
                ps.setBinaryStream(2, fin, fin.available());
                ps.executeUpdate();
            }
        }
    }

    public static String loadRentgen(File path, String druh, String jmeno, int vysetreniID) {//nahraje rentgen z databáze do počítače
        if(jmeno.equals("Vyberte Jméno"))
            return "Nevybrané jméno";
        if(druh.equals("Vyberte Druh")) {
            String[] temp = jmeno.split("\\)", 0);
            druh = temp[0].substring(1);
            jmeno = temp[1].substring(1);
        }
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select * from RENTEGENOVESNIMKY where VYSETRENIZVIRETEID = " + vysetreniID).executeQuery();
            int i = 0;
            while(rs.next()) {
                i++;
                Blob blob = rs.getBlob(3);
                byte[] barr = blob.getBytes(1, (int) blob.length());
                FileOutputStream fileOut;

                fileOut = new FileOutputStream(path + "\\" + druh + "_" + jmeno +"_ID" + vysetreniID + "_" + i + ".jpg");
                fileOut.write(barr);
                fileOut.close();
            }
            con.close();
            return "Úspěšně uloženo " + i + " rentgenů.";
        } catch(Exception e) {
            return "Chyba připojení";
        }
    }

    public static String[][] getPocetMladat() {//výpis počtu mláďat
        String[][] out = new String[0][2];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select * from POCET_MLADAT_MINULY_ROK").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][2];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(2);
                out[out.length-1][1] = rs.getString(1);
            }

            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR","ERROR","ERROR"}};
        }
    }

    public static String[][] getPocetSamcuSamic() {//výpis počtu samců a samic
        String[][] out = new String[0][3];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("select * from POCET_POHLAVI_DRUHU").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][3];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(1);
                out[out.length-1][1] = rs.getString(2);
                out[out.length-1][2] = rs.getString(3);
            }

            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR","ERROR","ERROR"}};
        }
    }

    public static String[][] getObsazenostVybehu() {//výpis obsazenosti výběhu
        String[][] out = new String[0][3];
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//ora1.uhk.cz:1521/orcl.uhk.cz", "brv1", "VaDaMa23");
            ResultSet rs = con.prepareStatement("SELECT * from obsazenost_vybehu").executeQuery();
            while(rs.next()) {
                String[][] temp = out.clone();
                out = new String[out.length+1][3];
                System.arraycopy(temp, 0, out, 0, temp.length);

                out[out.length-1][0] = rs.getString(1);
                out[out.length-1][1] = rs.getString(2);
                out[out.length-1][2] = rs.getString(3);
            }

            con.close();
            return out;
        } catch(Exception e) {
            return new String[][] {{"Chyba připojení","ERROR","ERROR"}};
        }
    }

    static boolean isTimeNoMinutes(String time) {//kontrola času
        if(time == null) {
            return false;
        }
        Pattern pattern = Pattern.compile("\\d\\d:\\d\\d");
        if(!pattern.matcher(time).matches()){
            return false;
        }
        int hours = Integer.parseInt(time.substring(0, 2));
        int minutes = Integer.parseInt(time.substring(3, 5));
        if(hours > 24 || hours < 0)
            return false;
        return minutes == 0;
    }

    static boolean isDate(String date) {//kontrola datumu
        int day;
        int month;
        int year;
        if(date == null) {
            return false;
        }
        Pattern pattern = Pattern.compile("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d");
        if(!pattern.matcher(date).matches())
            return false;
        day = Integer.parseInt(date.substring(0, 2));
        month = Integer.parseInt(date.substring(3, 5));
        year = Integer.parseInt(date.substring(6, 10));
        if(day <= 0 || day > 31 || month <= 0 || month > 12 || year <= 0 || year > LocalDate.now().getYear() + 1) {
            return false;
        }
        if((month == 4 || month == 6 || month == 9 || month == 11) && day > 30){
            return false;
        } else return (month != 2 || day <= 28 || year % 4 == 0) && (month != 2 || day <= 29);
    }
}